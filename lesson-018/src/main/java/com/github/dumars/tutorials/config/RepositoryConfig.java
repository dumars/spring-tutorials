package com.github.dumars.tutorials.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.Type;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

	@Autowired
	private EntityManager entityManager;

	/**
	 * 因為 spring data rest 遇到 @Id 這個 annotation 時，就不會顯示此欄位，
	 * 所以要額外設定才能正常顯示。
	 * @param config
	 */
	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		// @formatter:off
		config.exposeIdsFor(
				entityManager.getMetamodel()
						.getEntities()
						.stream()
						.map(Type::getJavaType)
						.toArray(Class[]::new));
		// @formatter:on
	}
}
