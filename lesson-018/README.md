# 服務註冊與發現

> 接下來要進入 Spring Cloud 的套件使用了，因為環境會開始變得更複雜所以 README 請詳細閱讀。

## 前言

Spring Cloud 在服務註冊與發現有幾種整合套件可以選擇，一個就是接下來說的 Spring Cloud Consul，
還有 Spring Cloud Netflix 中的 Eureka、ZooKepper 以及 Etcd，但就功能與實用性比較常見的就是在
Consul 與 Eureka 中選擇其中一種，要選擇的話要先了解其特性以及 CAP 中可以符合哪兩種特性。
  
[CAP定理](https://zh.wikipedia.org/wiki/CAP%E5%AE%9A%E7%90%86)
  
[套件比較表](https://blog.csdn.net/zyc88888/article/details/81453647)
  
## Consul

Consul 可以直接去下載執行程式並加上參數就可以使用，非常簡單而且又很輕量、提供的功能又很多。
直接執行的可以在官網文件看說明，下面要介紹的是用 Docker 啟動。

在 `src/documents/compose-consul.yml` 有簡單的設定，其中 volumes 的設定可以保留也可以移除，
要保留記得檢查資料路徑並修改。  
執行指令
```sh
docker-compose -f compose-consul.yml up -d
```

接下來試試 Consul REST API
```sh
curl http://127.0.0.1:8500/v1/agent/members
```
```json
[
	{
		"Name": "e66ab444d38f",
		"Addr": "127.0.0.1",
		"Port": 8301,
		"Tags": {
			"acls": "0",
			"build": "1.4.4:ea5210a3",
			"dc": "dc1",
			"id": "3f5ff9f0-5cb2-a73c-4fe5-6fbb765e490b",
			"port": "8300",
			"raft_vsn": "3",
			"role": "consul",
			"segment": "",
			"vsn": "2",
			"vsn_max": "3",
			"vsn_min": "2",
			"wan_join_port": "8302"
		},
		"Status": 1,
		"ProtocolMin": 1,
		"ProtocolMax": 5,
		"ProtocolCur": 2,
		"DelegateMin": 2,
		"DelegateMax": 5,
		"DelegateCur": 4
	}
]

```

在 yml 裡面的 command 中有加入 `-ui` 的參數，所以有 web ui 可以使用，
打開瀏覽器 [http://localhost:8500](http://localhost:8500)，
就可以看到 Consul 的操作介面。

接換到 `Key/Value` 的頁面，點擊 create

1. 第一個欄位(Key or folder) 中輸入 `config/lesson-018/data`，config 及 lesson-018 是資料夾，data 是檔案

2. 第二個大欄位(Value) 切換為 YAML 格式並輸入 `message: hello dumars`

上面的操作只是為了稍後的測試用，Consul 的介紹與操作就先到這，更深入的操作與使用需要各位自己去看文件。
  
## Spring Cloud Consul

#### Dependencies
```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-consul-config</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```
1. spring-cloud-starter-consul-discovery 是 Consul 的服務註冊與發現。

2. spring-cloud-starter-consul-config 是用來取得 Consul Key-Value 的機制。

3. spring-boot-starter-actuator 這個套件有很多可以說的，但是在這裡我們只要知道它會提供心跳檢查就夠了。

#### bootstrap.yml

```yml
server:
  port: 8080

spring:
  application:
    name: lesson-018
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        instance-id: ${spring.application.name}:${server.port}
        prefer-ip-address: true
        health-check-interval: 10s
        service-name: ${spring.application.name}
        enabled: true
        register: true
      config:
        enabled: true
        format: yaml
        prefix: config
        default-context: ${spring.application.name}
```

bootstrap.yml 的意義：當需要把一些初始設定在應用系統啟動前先準備好的時候，就可以將設定寫在 bootstrap.yml，
也就是這個檔案是在啟動應用系統前就會被載入，其中的操作也是在啟動之前，對照設定內容就可以知道，
在啟動之前會先到 Consul 註冊，並且會取得對應設定的 Config 內容。

`spring.cloud.consul.discovery` 的設定相對比較單純，除了要注意 instance-id 在多個 instance 的時候要避免一樣，
剩餘的也跟本章節無關暫時不說。

`spring.cloud.consul.config` 的設定就有很多變化了，這部分有興趣可以自行去了解。
  
#### application.yml

```yml
logging:
  level:
    root: WARN
    com.github.dumars: DEBUG

# password 記得要改
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/oauth?useSSL=false&useUnicode=true&characterEncoding=UTF-8
    username: root
    password: p@ssw0rd
    driver-class-name: com.mysql.jdbc.Driver
  jpa:
    show-sql: true
    open-in-view: true
    properties:
      hibernate:
        dialect: org.hibernate.dialect.MySQL5Dialect
        hbm2ddl:
          auto: update
```

#### Annotation EnableDiscoveryClient

```java
@SpringBootApplication
@EnableDiscoveryClient
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
```
  
---
  
## 執行

程式都是從 lesson-012 複製過來的，簡單的說我們要做的是啟動多個不同 port 的 lesson-018 web application，
然後透過 Discovery Client 的機制由 Consul 告訴我們該連接哪一個。

```sh
mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8080
mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8081
mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8082
```

請不要急著執行.....看完下一個章節再一起操作

