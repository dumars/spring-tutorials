# Layout and Webjars

本節介紹 Thymeleaf Layout 的使用方式及 Webjars 的使用

## Layout

在 JSP 中很常見的樣板(template)套件有 OpenSymphony SiteMesh、Apache Tiles...等，但是這只適合在 JSP 上使用，
在 Thymeleaf 上則有兩種常見的方式

### Fragment

建立一個基礎的頁面，然後在需要或可替換的區塊上標註 `th:fragment="fragment-name"`，然後在真正的畫面上
使用 `th:include` 或 `th:replace` 的方式來加入 fragment 的內容。

`fragments/template.html`
```html
... 略

<header th:fragment="header">
  <ul>
    <li>001</li>
    <li>002</li>
    <li>003</li>
  </ul>
</header>

... 略
```

`index.html`
```html
<html>
  <head>
    <meta charset="utf-8"/>
    <title>index</title>
  </head>
  <body>
    <div th:include="fragments/template :: header"></div>
    <div class="container">
      <h1>Lesson 007</h1>
    </div>
  </body>
</html>
```

以上是一個最簡單的例子，就是把 template.html 裡面的 header fragment 插入 index.html 裡面，
template.html 可以是一個結構完整的 html 內容，裡面預先定義很多各個位置的區塊供其他頁面取用，
而且因為是 html 格式，所以可以直接用瀏覽器開啟。

### layout:decorator

這是針對 Thymeleaf 的第三方套件，所以要加入新的 dependency
```xml
<dependency>
    <groupId>nz.net.ultraq.thymeleaf</groupId>
    <artifactId>thymeleaf-layout-dialect</artifactId>
</dependency>
```
在使用上也很簡單，首先建立一個完整的頁面

`layout/default.html`
```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" xmlns:sec="http://www.thymeleaf.org/extras/spring-security" lang="zh-Hant-TW">
<head th:fragment="head" th:remove="tag">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title layout:title-pattern="$CONTENT_TITLE">Main</title>
    <link th:href="@{/webjars/bootstrap/css/bootstrap.min.css}" rel="stylesheet">
</head>
<body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal">Company name</h5>
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="#">Features</a>
            <a class="p-2 text-dark" href="#">Enterprise</a>
            <a class="p-2 text-dark" href="#">Support</a>
            <a class="p-2 text-dark" href="#">Pricing</a>
        </nav>
        <a class="btn btn-outline-primary" href="/logout">Logout</a>
    </div>
    <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center" layout:fragment="main-header">
        <h1 class="display-4">功能名稱</h1>
        <p class="lead">簡單說明</p>
    </div>
    <div class="container" layout:fragment="main-content"></div>
    <script th:src="@{/webjars/jquery/jquery.min.js}"></script>
    <script th:src="@{/webjars/popper.js/popper.min.js}"></script>
    <script th:src="@{/webjars/bootstrap/js/bootstrap.min.js}"></script>
    <th:block layout:fragment="js-code"></th:block>
</body>
</html>
```

`item/creation.html`
```html
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{layout/default}">
<head>
    <title>Lesson 007</title>
</head>
<body>
    <div layout:fragment="main-header">
        <h1 class="display-4">Lesson 007</h1>
        <p class="lead">Create New Item</p>
    </div>
    <div layout:fragment="main-content">
        <form class="form" action="/item" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="name">商品名稱</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="商品名稱" maxlength="20" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="name">商品價格</label>
                    <input type="number" id="price" name="price" class="form-control" placeholder="商品價格" max="1000" required>
                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="儲存">
        </form>
    </div>
    <th:block layout:fragment="js-code">
        <!--/* your javascript */-->
        <script th:inline="javascript"></script>
    </th:block>
</body>
</html>
```

以上範例就完成了 `item/creation.html` 的樣板套用，至於 `item/detail.html` 就交給各位練習。

---

## Webjars

前端頁面不可避免的都需要用到 css、javascript 或是網頁字型，因此發展出了靜態 jar 的包裝方式，
透過修改引用的版號即可更換更新的套件，單純的 webjars 在宣告時還需要加入版號路徑，
其實這是對應 jar 裡面的資料夾路徑，所以這裡額外引用了 `webjars-locator` 就是可以省去版號這個路徑名稱，
方便以後更容易升級。

```xml
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>bootstrap</artifactId>
</dependency>
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>jquery</artifactId>
</dependency>
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>popper.js</artifactId>
</dependency>
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>webjars-locator</artifactId>
</dependency>
```

新版的 Spring 已經預設有支援 webjars 路徑，所以不用再像以前需要設定 resources 對應路徑，套件加入之後即可使用。

實際使用請看 `resources/templates/layout/default.html`