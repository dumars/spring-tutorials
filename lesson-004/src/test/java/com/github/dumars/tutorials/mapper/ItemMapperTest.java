package com.github.dumars.tutorials.mapper;

import com.github.dumars.tutorials.domain.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ItemMapperTest {

	@Autowired
	private ItemMapper itemMapper;

	@Test
	public void testOperation() {
		Item item = new Item();
		item.setName("雞腿便當");
		item.setPrice(100F);
		item.setCreatedBy(1L);
		item.setCreationDate(LocalDateTime.now());

		itemMapper.insert(item);
		assertNotNull(item.getId());

		itemMapper.selectByPrimaryKey(item.getId());
		assertEquals("雞腿便當", item.getName());
		assertEquals(Float.valueOf(100F), item.getPrice());

		item.setPrice(120F);
		itemMapper.updateByPrimaryKey(item);

		item = itemMapper.selectByPrimaryKey(item.getId());
		assertEquals(Float.valueOf(120F), item.getPrice());

		Long id = item.getId();
		itemMapper.deleteByPrimaryKey(item.getId());
		assertNull(itemMapper.selectByPrimaryKey(id));
	}
}