package com.github.dumars.tutorials.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.AutoConfigureMybatis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMybatis
public class FirstFormControllerTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void index() throws Exception {
		this.mvc.perform(get("/item"))
				.andExpect(status().isOk())
				.andExpect(view().name("/item/creation"));
	}
}