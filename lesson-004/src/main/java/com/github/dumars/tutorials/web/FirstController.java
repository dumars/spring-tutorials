package com.github.dumars.tutorials.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Spring MVC.
 *
 * @author dumars
 */
@Controller
public class FirstController {

	/**
	 * <code>@GetMapping</code> 亦同 <code>@RequestMapping(method = RequestMethod.GET)</code>
	 * <code>@GetMapping</code> 也就是只接受 GET 的方式來操作此方法
	 *
	 * @return 讀取 /resources/templates/index.html
	 */
	@GetMapping("/")
	public String index() {
		return "index";
	}
}
