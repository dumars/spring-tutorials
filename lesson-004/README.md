# Spring + MyBatis 整合

這裡會使用 lesson 003 中的 mysql 資料庫  

> 在開始前請先建立一個 mysql database，名字叫做 oauth，下面的資料庫連線(spring.datasource.url)會使用 oauth 這個資料庫，或者請改成自己的資料庫名稱

## MyBatis

- MyBatis 有提供 spring-boot 的 auto configuration 的套件，所以使用上非常的簡單，先把套件加入 maven。  

```yml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```
---

- 加入 spring datasource 設定到 application.yml，密碼部分記得換掉

```yml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/oauth?useSSL=false&useUnicode=true&characterEncoding=UTF-8
    username: root
    password: 123456
    driver-class-name: com.mysql.jdbc.Driver
```

> 在 lesson-004 下面有個 database 資料夾，裡面有建立 Item Table 的指令，請記得建立 Table。

---

- 加入 mybatis 基本設定到 application.yml  

```yml
mybatis:
  type-aliases-package: com.github.dumars.tutorials.model
  mapper-locations: classpath:mapper/*.xml
  configuration:
    map-underscore-to-camel-case: true
    default-fetch-size: 100
    default-statement-timeout: 30
```
---

PS: MyBatis 的各種細節請參考官方網站，這裡只簡單介紹整合的部分。  

## 程式說明

因為使用 mybatis-spring-boot-starter，所以會去找有 `@Mapper` 的 annotation 然後加入 spring bean，
所以實際上需要的設定很少(有額外設定需求請參考官方網站)，剩下的就是使用 Mapper 對資料庫操作。