# Lesson 003: Docker

> 怎麼在這裡突然跳出 Docker 這個議題，因為目前對開發人員而言，Docker 越來越重要。

1. 安裝 Docker
2. 檢查設定，例如：使用 Windows 環境的要設定 CPU、記憶體的分配
3. 接下來裝個 mysql 練練手

## mysql

先到 [docker hub](https://hub.docker.com/) 搜尋 mysql，可以找到官方提供的 images
```sh
# 不一定要下載最新的，練手而已就用自己熟悉的就夠了
docker pull mysql:5.7.26

# 啟動 mysql，記得下方的密碼要換掉
docker run -p 3306:3306 --name mysql -v d:/cloud/data/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7.26 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

# 檢查容器狀態
docker ps -a

# 或看一下 log，指令最後面的 mysql 是對應你剛剛給的名稱(--name 指定的)，或是換成 container id(上一個指令可以看到)
docker logs -f mysql
```

> 記得修改資料夾路徑以符合自己的環境

或是使用 docker-compose 的方式來管理容器

建立 compose-mysql.yml
```yml
version: "3.3"

services:
  mysql:
    image: "mysql:5.7.26"
    container_name: mysql
    command: --default-authentication-plugin=mysql_native_password --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
    volumes:
      - "D:/cloud/data/mysql:/var/lib/mysql"
    ports:
      - "3306:3306"
    environment:
      - "MYSQL_ROOT_PASSWORD=123456"

```

指令：
```sh
# 啟動並保持在背景執行
docker-compose -f compose-mysql.yml up -d

# 停止運行並移除此容器
docker-compose -f compose-mysql.yml down

# 其他簡單操作
docker start mysql
docker stop mysql
docker restart mysql

```

以上的操作就非常簡單的啟動了 mysql 資料庫服務。