package com.github.dumars.tutorials.config;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.resource.ClientResources;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@Configuration
@EnableRedisHttpSession
public class StandaloneRedisConfig {

	@Autowired
	private RedisProperties properties;

	@Bean
	ClientOptions clientOptions() {
		return ClientOptions.builder()
				.disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS)
				.autoReconnect(true).build();
	}

	@Bean
	RedisStandaloneConfiguration redisStandaloneConfiguration() {
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(
				properties.getHost(), properties.getPort());
		config.setDatabase(properties.getDatabase());
		config.setPassword(properties.getPassword());
		return config;
	}

	@Bean
	LettucePoolingClientConfiguration lettucePoolConfig(ClientResources clientResources) {
		GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
		poolConfig.setMaxIdle(properties.getLettuce().getPool().getMaxIdle());
		poolConfig.setMinIdle(properties.getLettuce().getPool().getMinIdle());
		poolConfig.setMaxTotal(properties.getLettuce().getPool().getMaxActive());
		poolConfig.setMaxWaitMillis(properties.getLettuce().getPool().getMaxWait().toMillis());
		// @formatter:off
		return LettucePoolingClientConfiguration.builder()
				.poolConfig(poolConfig)
				.clientOptions(clientOptions())
				.clientResources(clientResources)
				.build();
		// @formatter:on
	}

	@Bean
	RedisConnectionFactory redisConnectionFactory(LettucePoolingClientConfiguration lettucePoolConfig) {
		return new LettuceConnectionFactory(redisStandaloneConfiguration(), lettucePoolConfig);
	}

	@Bean
	StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
		return new StringRedisTemplate(redisConnectionFactory);
	}

	@Bean
	public RedisTemplate<?, ?> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		RedisTemplate<byte[], byte[]> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}
}