package com.github.dumars.tutorials.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@RedisHash("item")
public class Item {

	@Id
	private Long id;
	private String name;
	private Float price;

}
