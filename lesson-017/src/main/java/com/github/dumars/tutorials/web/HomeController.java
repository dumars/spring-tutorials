package com.github.dumars.tutorials.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

	@GetMapping("/")
	public String index(HttpSession session) {
		if(session.isNew()) {
			session.setAttribute("foo", "123");
		}
		return "index";
	}

	@PostMapping("/")
	public String update(String fooValue, HttpSession session) {
		if (fooValue != null && fooValue.length() > 0) {
			session.setAttribute("foo", fooValue);
		}
		return "index";
	}
}
