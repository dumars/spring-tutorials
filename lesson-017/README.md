# Spring Session Data Redis

延續上一個章節的課題 Redis，這一節是將 Session 也加入到 Redis 中，實做過程非常簡單，
將上一節的 spring-data-redis 換成 spring-session-data-redis，要注意的是兩者的
group_id 是不同的喔。

```xml
<!-- 原有的 -->
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-redis</artifactId>
</dependency>

<!-- 換成這個 -->
<dependency>
    <groupId>org.springframework.session</groupId>
    <artifactId>spring-session-data-redis</artifactId>
</dependency>
```

並加上一個設定 `@EnableRedisHttpSession`

```java
@Configuration
@EnableRedisHttpSession
public class StandaloneRedisConfig {
    ...
}
```

這樣就大功告成了，喝口水的時間就把 session 放到 redis 上了。

實際使用跟你以前用 session 的方式一模一樣，不用修改任何程式。

## Cookie Name
  
```yml
server:
  servlet:
    session:
      cookie:
        name: LESSON-017
```

這邊順便示範怎麼變更 cookie name，執行系統並打開頁面後就可以在瀏覽器看看 cookie name 是否為 `LESSON-017`