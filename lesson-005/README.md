# Spring Properties 的運用

YAML 的設定方式，在 application.yml 中
```yml
lesson:
  data:
    my-number: 100
    my-text: Hello
```
等同以前常用的 application.properties
```properties
lesson.data.myNumber=100
lesson.data.myText=Hello
```

---

## `@Value("${key}")`

使用 `@Value("${lesson.data.myNumber}")` 或 `@Value("${lesson.data.my-number}")`
都可以取得設定檔中的設定值，亦可以給予初始值，例如：`@Value("${lesson.data.my-number:50}")`，
如果設定檔中有此項目則會以設定檔的值為主，反之則以初始值為主。


## `@ConfigurationProperties(prefix = "lesson.data")`

`@ConfigurationProperties` 需搭配 `@EnableConfigurationProperties` Spring 才能知道你要用此方式載入設定
```java
@SpringBootApplication
@EnableConfigurationProperties(LessonProperties.class)
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
```

這種方式可以很方便的載入大數量的設定值，並且可以搭配此方式作多種的設定搭配

---

## Profiles

Spring 針對不同環境或使用情境可以設定多組的 profiles，搭配 `@Profile`、`@ActiveProfiles` 等等 annotation 更可以有彈性的
設定多種情境，這部分就由各位自行去學習。