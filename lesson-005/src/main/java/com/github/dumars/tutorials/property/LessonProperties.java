package com.github.dumars.tutorials.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "lesson.data")
public class LessonProperties {

	private int myNumber;

	private String myText;
}
