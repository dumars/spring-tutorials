package com.github.dumars.tutorials.web;

import com.github.dumars.tutorials.property.LessonProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Spring MVC.
 *
 * @author dumars
 */
@Controller
public class FirstController {

	@Value("${lesson.data.my-number}")
	private int myNumber;
	@Value("${lesson.data.my-text}")
	private String myText;

	@Autowired
	private LessonProperties properties;

	/**
	 * <code>@GetMapping</code> 亦同 <code>@RequestMapping(method = RequestMethod.GET)</code>
	 * <code>@GetMapping</code> 也就是只接受 GET 的方式來操作此方法
	 *
	 * @return 讀取 /resources/templates/index.html
	 */
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("myNumber", myNumber);
		model.addAttribute("myText", myText);
		model.addAttribute("properties", properties);
		return "index";
	}
}
