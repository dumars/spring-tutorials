# Spring Handler Interceptor

Spring Interceptor 在實做上有幾種選擇：

1. implements HandlerInterceptor

2. extends HandlerInterceptorAdapter

3. extends WebRequestHandlerInterceptorAdapter

4. ....

有好多種方式，有些是針對某種類型的需求，詳細可以去看 API 文件。
後面只針對 HandlerInterceptor 來實做。

---
> 假設我們系統經常要顯示一些操作過程產生的各種提示訊息，所以可能在 Controller、Service...各個地方都有需要收集訊息，然後最後顯示在畫面上；當然純手工每個 Controller 一開始就建立一個 List 然後沿路傳遞下去不停往裡面塞訊息最後丟出去這也是一種方法，但是有沒有比較優雅的方式呢？


先建立一個訊息載體(陽春版)
```java
public class Messages implements Serializable {

   private final List<String> messages;

   public Messages() {
      this.messages = new ArrayList<>();
   }

   public void add(String message) {
      messages.add(message);
   }

   @NonNull
  public List<String> getMessages() {
      return messages;
   }

   public boolean isEmpty() {
      return messages.isEmpty();
   }

   public int size() {
      return messages.size();
   }

   public String toString() {
      if (!isEmpty()) {
         StringBuilder builder = new StringBuilder();
         messages.forEach(message -> builder.append("<div>").append(message).append("</div>"));
         return builder.toString();
      }
      return null;
   }

   public void destroy() {
      messages.clear();
   }
}
```


然後把它當成一個 Scope = request 的 Spring Bean
```java
@Bean(destroyMethod = "destroy")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
Messages messages() {
   return new Messages();
}
```


要使用時就直接 `@Autowired`，就不需要從頭傳到尾，然後再透過 Interceptor 在 postHandler 將訊息內容加入 ModelAndView

```java
public class MessageInterceptor implements HandlerInterceptor {

   @Autowired
  private Messages messages;

   public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
      if (modelAndView != null && !modelAndView.isEmpty()) {
         modelAndView.addObject("defaultMessageObject", messages);
      }
   }
}
```


再將 MessageInterceptor 加入 Spring Interceptor 中
```java
@Bean
MessageInterceptor messageInterceptor() {
   return new MessageInterceptor();
}

@Override
public void addInterceptors(InterceptorRegistry registry) {
   registry.addInterceptor(messageInterceptor());
}
```


在頁面上(Thymeleaf)使用 layout 的方式寫在 non-fragment 的地方，這樣工程師就不用每次都要自己處理訊息

`layout/default.html`
```html
<div th:if="${defaultMessageObject != null && defaultMessageObject.size() > 0}" class="alert alert-warning" role="alert" style="position:absolute;bottom:0px;">
    <th:block th:utext="${defaultMessageObject}"></th:block>
</div>
```

這樣一個非常陽春的訊息顯示就完成了。


## 後記
在 Message 保存與傳遞這一塊，其實使用 `ThreadLocal` 會比較好，網路上也可以找到大量的教學、解說文章，有興趣可以試試把這部分轉換成 `ThreadLocal` 的方式儲存。

另外 HandlerInterceptor 還被大量的用在 Log 紀錄、認證及授權檢查，或者其他重複類型高的代碼中，這個一樣可以在網路上找到大量的文章。