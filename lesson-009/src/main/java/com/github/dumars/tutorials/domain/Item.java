package com.github.dumars.tutorials.domain;

import lombok.Data;

@Data
public class Item {

	private Long id;
	private String name;
	private Float price;

}
