package com.github.dumars.tutorials.config;

import com.github.dumars.tutorials.interceptor.MessageInterceptor;
import com.github.dumars.tutorials.interceptor.Messages;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	/**
	 * 直接將網址路徑與 View 做匹配
	 * @param registry
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
	}

	@Bean
	MessageInterceptor messageInterceptor() {
		return new MessageInterceptor();
	}

	/**
	 * 加入 MessageInterceptor
	 * @param registry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(messageInterceptor());
	}

	@Bean(destroyMethod = "destroy")
	@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
	Messages messages() {
		return new Messages();
	}
}