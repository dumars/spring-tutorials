package com.github.dumars.tutorials.interceptor;

import org.springframework.lang.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Messages implements Serializable {

	private final List<String> messages;

	public Messages() {
		this.messages = new ArrayList<>();
	}

	public void add(String message) {
		messages.add(message);
	}

	@NonNull
	public List<String> getMessages() {
		return messages;
	}

	public boolean isEmpty() {
		return messages.isEmpty();
	}

	public int size() {
		return messages.size();
	}

	public String toString() {
		if (!isEmpty()) {
			StringBuilder builder = new StringBuilder();
			messages.forEach(message -> builder.append("<div>").append(message).append("</div>"));
			return builder.toString();
		}
		return "";
	}

	public void destroy() {
		messages.clear();
	}
}
