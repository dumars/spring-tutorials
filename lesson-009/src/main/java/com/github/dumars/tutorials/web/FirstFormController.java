package com.github.dumars.tutorials.web;

import com.github.dumars.tutorials.domain.Item;
import com.github.dumars.tutorials.interceptor.Messages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Spring MVC and first form.
 *
 * @author dumars
 */
@Slf4j
@Controller
public class FirstFormController {

	@Autowired
	private Messages messages;

	/**
	 * 開啟新增商品的輸入表單
	 * @return
	 */
	@GetMapping("/item")
	public String index() {
		log.debug("into item index page.");
		messages.add("進入新增商品操作畫面");
		messages.add("每個欄位都要填");
		return "item/creation";
	}

	/**
	 * 儲存商品資料，因為使用了 redirect 機制，所以須使用 RedirectAttributes 來將資料帶到下一個環節
	 * @param item 商品內容
	 * @param attributes {@Link RedirectAttributes}
	 * @return
	 */
	@PostMapping("/item")
	public String save(Item item, Messages messages, RedirectAttributes attributes) {
		item.setId(1L);
		attributes.addFlashAttribute("item", item);
		return "redirect:/item/1";
	}

	/**
	 * 顯示商品明細
	 * @param id
	 * @return
	 */
	@GetMapping("/item/{id}")
	public String findItemById(@PathVariable("id") Long id) {
		log.debug("find item by id: {}", id);
		return "item/detail";
	}
}
