package com.github.dumars.tutorials.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MessageInterceptor implements HandlerInterceptor {

	@Autowired
	private Messages messages;

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
		if (modelAndView != null && !modelAndView.isEmpty()) {
			modelAndView.addObject("defaultMessageObject", messages);
		}
	}
}
