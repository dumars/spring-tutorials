package com.github.dumars.tutorials.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * Security 設定.
 *
 * @author dumars
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * 設定 {@Link HttpSecurity} 規則
	 * @param http
	 * @throws Exception
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.authorizeRequests()
				// 設定所有的 Request 都需要有認證過(已登入)
				.anyRequest().authenticated()
				.antMatchers("/item/**").hasAnyRole("USER", "ADMIN")
				.and()
			// 啟用內建的登入畫面
			.formLogin()
				.and()
			// 提供 basic access authentication 方式，請看 README
			.httpBasic();
		// @formatter:on
	}

	/**
	 * 將不用做身分與角色驗證的路徑規則加入
	 * @param web {@link WebSecurity}
	 * @throws Exception
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/**", "/js/**", "/images/**");
	}

	/**
	 * Spring Security 5 的密碼編碼方式，支援非常多種編碼方式，預設為 "bcrypt"
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		// @formatter:off
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();

		manager.createUser(User
				.withUsername("dumars")
				.password(passwordEncoder().encode("dumars"))
				.roles("USER")
				.build());

		manager.createUser(User
				.withUsername("admin")
				.password(passwordEncoder().encode("admin"))
				.roles("ADMIN")
				.build());

		return manager;
		// @formatter:on
	}

}
