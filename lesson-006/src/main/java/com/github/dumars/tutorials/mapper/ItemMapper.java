package com.github.dumars.tutorials.mapper;

import com.github.dumars.tutorials.domain.Item;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ItemMapper {

	@Delete({
			"delete from item",
			"where id = #{id,jdbcType=BIGINT}"
	})
	int deleteByPrimaryKey(Long id);

	@Insert({
			"insert into item (created_by, creation_date, ",
			"name, price, update_date, ",
			"updated_by)",
			"values (#{createdBy,jdbcType=BIGINT}, #{creationDate,jdbcType=TIMESTAMP}, ",
			"#{name,jdbcType=VARCHAR}, #{price,jdbcType=REAL}, #{updateDate,jdbcType=TIMESTAMP}, ",
			"#{updatedBy,jdbcType=BIGINT})"
	})
	@SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
	int insert(Item record);

	int insertSelective(Item record);

	@Select({
			"select",
			"id, created_by, creation_date, name, price, update_date, updated_by",
			"from item",
			"where id = #{id,jdbcType=BIGINT}"
	})
	@ResultMap("com.github.dumars.tutorials.mapper.ItemMapper.BaseResultMap")
	Item selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(Item record);

	@Update({
			"update item",
			"set created_by = #{createdBy,jdbcType=BIGINT},",
			"creation_date = #{creationDate,jdbcType=TIMESTAMP},",
			"name = #{name,jdbcType=VARCHAR},",
			"price = #{price,jdbcType=REAL},",
			"update_date = #{updateDate,jdbcType=TIMESTAMP},",
			"updated_by = #{updatedBy,jdbcType=BIGINT}",
			"where id = #{id,jdbcType=BIGINT}"
	})
	int updateByPrimaryKey(Item record);

	void batchInsert(List<Item> items);
}