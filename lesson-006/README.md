# Spring Security

Spring Security 針對 Servlet 與 WebFlux 有不同的對應程式，所以使用時需注意，不過兩者之間很容易區別；通常帶有 Reactive 的都是使用在 WebFlux 的，所以注意一下就沒問題。

## Modules
1. Core - spring-security-core.jar
2. Remoting - spring-security-remoting.jar
3. Web - spring-security-web.jar
4. Config - spring-security-config.jar
5. LDAP - spring-security-ldap.jar
6. OAuth 2.0 Core - spring-security-oauth2-core.jar
7. OAuth 2.0 Client - spring-security-oauth2-client.jar
8. OAuth 2.0 JOSE - spring-security-oauth2-jose.jar
9. ACL - spring-security-acl.jar
10. CAS - spring-security-cas.jar
11. OpenID - spring-security-openid.jar
12. Test - spring-security-test.jar

Spring Security 支援多種的認證方式，因此使用時只要選擇適用的模組並搭配簡單的設定即可簡單的完成安全設定。
這部分要自己去看官方文件，裡面有完整的說明。需要注意的是在 maven 設定時，有無使用 spring boot 時其設定是不同的。

## 認證與授權
應用級別的安全主要分為`驗證(authentication)`和`授權(authorization)`兩個部分，
這也是Spring Security主要需要處理的兩個部分。

- `Authentication`指的是建立規則 (principal) 的過程。規則可以是一個使用者、裝置、
或者其他可以在我們的應用中執行某種操作的其他系統。

- `Authorization`指的是判斷某個 principal 在我們的應用是否允許執行某個操作。
在進行授權判斷之前，要求其所要使用到的規則必須在驗證過程中已經建立好了。

## WebSecurityConfigurerAdapter

```java
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
}
```

### Annotation 說明

- `@EnableWebSecurity` 啟用 spring security 的某些設定，哪些呢？
```java
@Import({ WebSecurityConfiguration.class,
		SpringWebMvcImportSelector.class,
		OAuth2ImportSelector.class })
@EnableGlobalAuthentication
```
看 Class Name 大概就能知道有一些 web security 的基本設定，mvc 相關的設定值以及 OAuth2 的東西，
大概知道這些就暫時夠了，這裡是幼幼班教學，不是原始碼解析。

- `@EnableGlobalMethodSecurity(prePostEnabled = true)` 則是下個章節的內容(Method Security)，
這邊先略過。

### 繼承 WebSecurityConfigurerAdapter

這裡面寫明了很多驗證的流程，而且因為 spring security 支援多種認證方式，所以除了一些 DefaultXxxx 的物件
就是 Interface，我們只是要做最簡單的練習，所以看 `WebSecurityConfig` 的說明吧。

### Basic access authentication

這種認證方式一定要了解一下，詳細說明請看 Wiki [HTTP基本認證](https://zh.wikipedia.org/wiki/HTTP%E5%9F%BA%E6%9C%AC%E8%AE%A4%E8%AF%81)

## Method Security

spring 中用`@PreAuthorize`標註在方法上，利用 spring 的切面思想，
在方法呼叫前先進行許可權驗證匹配，如果合適再進行方法的呼叫。

因為是標註在方法上，所以可以在 Controller、Service、Repository.... 的方法中加入授權檢驗。

## InMemoryUserDetailsManager

看名字就知道了，放在記憶體中的使用者資料，在這裡我們建立兩組帳號

- dumars:dumars
- admin:admin

在目前的程式中沒有做額外的變化，這兩組帳號能做的事情是一樣的，接下來的功課就是利用這兩組帳號來限制可以使用的方法或URI

## Thymeleaf

在 Maven 中還加入 Thymeleaf 的額外套件
```xml
<dependency>
    <groupId>org.thymeleaf.extras</groupId>
    <artifactId>thymeleaf-extras-springsecurity5</artifactId>
</dependency>
```
這是 Thymeleaf 針對 Spring Security 所增加的額外功能，詳細請看(內容很少也很簡單請放心服用) [官方說明](https://www.thymeleaf.org/doc/articles/springsecurity.html)
