package com.github.dumars.tutorials.rest;

import com.github.dumars.tutorials.domain.Item;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ItemRestControllerTest {

	@LocalServerPort
	private int randomServerPort;

	private String prefixUrl;

	@Before
	public void setup() {
		prefixUrl = "http://localhost:" + randomServerPort;
	}

	@Test
	public void findById() {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
		ResponseEntity<Item> entity = testRestTemplate.getForEntity(prefixUrl + "/item/1", Item.class);
		assertNotNull(entity);
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		assertNotNull(entity.getBody());
	}
}