package com.github.dumars.tutorials.rest;

import com.github.dumars.tutorials.domain.Item;
import com.github.dumars.tutorials.mapper.ItemMapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/item")
@ExposesResourceFor(Item.class)
public class ItemRestController {

	@Autowired
	private ItemMapper itemMapper;

	@GetMapping("/{id}")
	public Item findById(@PathVariable("id") Long id) {
		return itemMapper.selectByPrimaryKey(id);
	}

	@GetMapping("/{id}/hateoas")
	public ResponseEntity findByIdWithHateoas(@PathVariable("id") Long id) {
		Item item = itemMapper.selectByPrimaryKey(id);
		Resource<Item> resource = new Resource<>(item);
		resource.add(linkTo(ItemRestController.class).slash(item.getId()).slash("hateoas").withSelfRel());
		return ResponseEntity.ok(resource);
	}

	@GetMapping
	public List<Item> findAll(Pageable pageable) {
		RowBounds rowBounds = new RowBounds(pageable.getPageNumber() * pageable.getPageSize(), pageable.getPageSize());
		return itemMapper.selectAll(rowBounds);
	}

	@GetMapping("/hateoas")
	public ResponseEntity findAllByHateoas(Pageable pageable) {
		RowBounds rowBounds = new RowBounds(pageable.getPageNumber() * pageable.getPageSize(), pageable.getPageSize());
		List<Item> items = itemMapper.selectAll(rowBounds);

		final List<Resource> list = new ArrayList<>();
		items.forEach(item -> {
			Resource<Item> resource = new Resource<>(item);
			resource.add(linkTo(ItemRestController.class).slash(item.getId()).slash("hateoas").withSelfRel());
			list.add(resource);
		});

		Resources<Resource> resources = new Resources<>(list);
		resources.add(linkTo(ItemRestController.class).slash("hateoas").withSelfRel());

		return ResponseEntity.ok(resources);
	}
}
