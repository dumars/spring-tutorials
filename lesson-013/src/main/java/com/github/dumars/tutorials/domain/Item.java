package com.github.dumars.tutorials.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.hateoas.Identifiable;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Item implements Identifiable<Long>, Serializable {

	private Long id;

	private String name;

	private Float price;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime creationDate;

	private Long createdBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateDate;

	private Long updatedBy;

}
