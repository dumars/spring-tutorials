# 來寫個 REST API 吧(MyBatis 版本)

MyBatis 並沒有被加入 Spring Data 的項目中，因此沒有像 JPA 這麼方便的整合體驗，
雖然網路上也能找到類似的項目 spring-data-mybatis-boot-starter，但是看項目的 issue 回報
似乎還有些問題，有興趣的可以自己試試看，畢竟很容易就可以加入項目中。

## Annotation RestController vs Controller

打開原始碼你會發現這兩者其實只有 `@ResponseBody` 的差別，因此你要直接用 `@RestController`
或是用 `@Controller` + `@ResponseBody` 的方式都是可以的。

## HATEOAS

程式中有套用簡單的 HATEOAS 的機制 ~~(有點麻煩...)~~ 

## Return Type

返回資料的類型，正常的物件(Item、List&lt;Item&gt;...等等)可以很正常的顯示，但是可操作空間比較小；
ResponseEntity 則可以根據各種條件或需求來加工，例如在 Headers 加入資訊，或是直接返回不同 Status Code，
不一定要把所以內容都放在 Body 中。

---

怎麼呼叫 API 並接收結果以及資料的轉換這部分的內容我打算放在更後面，
在 Cloud 的部分一起介紹，因為實在很簡單，或是自己看看 RestTemplate。