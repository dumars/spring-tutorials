# Lesson 2: Form and Log

這次練習開發中很常見的表單操作，以及了解什麼是 REST，並且簡單的使用 lombok、spring boot logging 設定及 DevTools

## REST

WIKI 上是這樣說的： [Representational state transfer](https://en.wikipedia.org/wiki/Representational_state_transfer)

這些內容不需要去背，只是要去看看這到底是什麼，簡單的了解一下，這對網站開發人員而言都是基本常識，
有這概念然後在之後開發的時候規劃網址路徑時多思考一下，很容易就會慢慢理解其用意了。

## lombok

[官方網站](https://projectlombok.org/)

這是一個非常好用的小工具，你可以常常在很多的開源套件的原始碼中看到，所以看一下說明並在你的開發環境中完成安裝。

## log

搭配 lombok 的話，在程式中非常容易使用，`@Slf4j` 使用方式如下：
```java
@Slf4j
@Controller
public class FirstFormController {
	
	@GetMapping("/item/{id}")
    public String findItemById(@PathVariable Long id) {
        log.debug("find item by id: {}", id);
        return "/item/detail";
    }
}
```

在 spring-boot-start-web 包含了 spring-boot-start，而 spring-boot-start 又包含了 spring-boot-start-logging，
所以我們可以直接使用 spring 的 log 設定，設定時在太簡單了 BJ4
```yml
logging:
  level:
    root: WARN
    com.github.dumars: DEBUG
```

要額外擴展 log 機制的話，就要增加 logback 的設定檔，去做額外的設定，有興趣可以自己 Google。

## Spring DevTools

DevTools 看名字就大概能猜到其用途，就是在開發期間幫助開發人員的一些額外設定與機制，詳細內容請參考 Google 或下面網址

[官方內容](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-devtools.html)

[Spring DevTools 介紹](https://blog.csdn.net/isea533/article/details/70495714)

## 表單操作

這部分請看程式碼 BJ4