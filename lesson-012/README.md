# 來寫個 REST API 吧(JPA 版本)

我們很多時候都要寫最基本的資料庫 CRUD 操作，大部分時候都是非常簡單的操作，
但是你還是不得不建立重複性非常高的 CRUDController，所以避免再浪費生命，
在你的 Maven 設定上加入

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-rest</artifactId>
</dependency>
```

然後再把 `@Repository` 換成 `@RepositoryRestResource` 這樣我們就瞬間擁有 Table 的基本操作了。

因為這次是用 JPA，所以 Maven 設定要拿掉 MyBatis 並加入 JPA 套件

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
```

## Annotation RepositoryRestResource

```java
@RepositoryRestResource(path = "item")
public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {

	@RestResource(exported = false)
	void deleteById(Long id);

	@RestResource(exported = false)
	void delete(Item item);

	@RestResource(exported = false)
	void deleteAll(Iterable<? extends Item> items);

	@RestResource(exported = false)
	void deleteAll();
}
```

`@RestResource(exported = false)` 的部分表示我們禁用所有基本的 delete 操作，其他查詢都可正常使用。

## PagingAndSortingRepository

因為繼承了 PagingAndSortingRepository 所以會有預設的分頁查詢跟排序功能功能。

## 操作(curl 指令或使用 postman)

使用 curl 指令：`curl http://localhost:8080`，得到以下結果
```json
{
  "_links" : {
    "items" : {
      "href" : "http://localhost:8080/item{?page,size,sort}",
      "templated" : true
    },
    "profile" : {
      "href" : "http://localhost:8080/profile"
    }
  }
}
```
  
  
分頁查詢：`curl http://localhost:8080/item?page=0&size=5`，得到以下結果
```json
{
    "_embedded": {
        "items": [
            {
                "id": 1,
                "name": "德國百靈-新升級三鋒系列電鬍刀",
                "price": 13337,
                "creationDate": "2019-04-20 16:56:16",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/1"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/1"
                    }
                }
            },
            {
                "id": 2,
                "name": "飛利浦 Shaver series 三刀頭可水洗電鬍刀",
                "price": 15869,
                "creationDate": "2019-04-20 16:56:16",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/2"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/2"
                    }
                }
            },
            {
                "id": 3,
                "name": "ALCHEMA 智慧釀酒機",
                "price": 14015,
                "creationDate": "2019-04-20 16:56:16",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/3"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/3"
                    }
                }
            },
            {
                "id": 4,
                "name": "日本麗克特 Solen 果汁機-櫻花粉限定款",
                "price": 8001,
                "creationDate": "2019-04-20 16:56:16",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/4"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/4"
                    }
                }
            },
            {
                "id": 5,
                "name": "伊萊克斯設計家系列桌上型抬頭式攪拌機",
                "price": 9813,
                "creationDate": "2019-04-20 16:56:16",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/5"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/5"
                    }
                }
            }
        ]
    },
    "_links": {
        "first": {
            "href": "http://localhost:8080/item?page=0&size=5"
        },
        "self": {
            "href": "http://localhost:8080/item{&sort}",
            "templated": true
        },
        "next": {
            "href": "http://localhost:8080/item?page=1&size=5"
        },
        "last": {
            "href": "http://localhost:8080/item?page=32&size=5"
        },
        "profile": {
            "href": "http://localhost:8080/profile/item"
        },
        "search": {
            "href": "http://localhost:8080/item/search"
        }
    },
    "page": {
        "size": 5,
        "totalElements": 161,
        "totalPages": 33,
        "number": 0
    }
}
```
  
  
單筆查詢：`http://localhost:8080/item/1`，得到以下結果
```json
{
    "id": 1,
    "name": "德國百靈-新升級三鋒系列電鬍刀",
    "price": 13337,
    "creationDate": "2019-04-20 16:56:16",
    "createdBy": 1,
    "updateDate": null,
    "updatedBy": null,
    "_links": {
        "self": {
            "href": "http://localhost:8080/item/1"
        },
        "item": {
            "href": "http://localhost:8080/item/1"
        }
    }
}
```

使用自己寫的方法(findByNameStartsWith)：`http://localhost:8080/item/search/nameStartsWith?name=A&sort=id,asc`
```json
{
    "_embedded": {
        "items": [
            {
                "id": 3,
                "name": "ALCHEMA 智慧釀酒機",
                "price": 14015,
                "creationDate": "2019-04-20 16:56:16",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/3"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/3"
                    }
                }
            },
            {
                "id": 14,
                "name": "ALCHEMA 智慧釀酒機",
                "price": 2281,
                "creationDate": "2019-04-20 16:56:36",
                "createdBy": 1,
                "updateDate": null,
                "updatedBy": null,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/item/14"
                    },
                    "item": {
                        "href": "http://localhost:8080/item/14"
                    }
                }
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/item/search/nameStartsWith?name=A&page=0&size=20&sort=id,asc"
        }
    },
    "page": {
        "size": 20,
        "totalElements": 2,
        "totalPages": 1,
        "number": 0
    }
}
```

其他說明請看程式內註解。

### 額外補充

在 spring-boot-starter-data-rest 中包含
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-hateoas</artifactId>
</dependency>
```

這個套件就是讓回傳結果中多了很多額外的資訊，而並非只有單純的資料。

HATEOAS 的說明請參考這個網站 [在 Spring Boot 中使用 HATEOAS](https://aisensiy.github.io/2017/06/04/spring-boot-and-hateoas/) 或自行 Google。