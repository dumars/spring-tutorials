package com.github.dumars.tutorials.repository;

import com.github.dumars.tutorials.domain.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * 商品資料操作.
 * 將 Repository 當作 Resource 使用.
 *
 * @author dumars
 */
@RepositoryRestResource(path = "item", itemResourceDescription = @Description("商品資料查詢"))
public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {

	@RestResource(path = "nameStartsWith", rel = "nameStartsWith")
	public Page findByNameStartsWith(@Param("name") String name, Pageable pageable);

	@RestResource(exported = false)
	void deleteById(Long id);

	@RestResource(exported = false)
	void delete(Item item);

	@RestResource(exported = false)
	void deleteAll(Iterable<? extends Item> items);

	@RestResource(exported = false)
	void deleteAll();
}