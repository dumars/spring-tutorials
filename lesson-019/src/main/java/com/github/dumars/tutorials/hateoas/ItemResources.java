package com.github.dumars.tutorials.hateoas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.dumars.tutorials.domain.Item;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemResources {

	@JsonProperty("_embedded")
	private Root root;

	@JsonProperty("page")
	private Pagination pagination;

	@Data
	public static class Root {

		@JsonProperty("items")
		private List<Item> items;

		public Root() {
			this.items = new ArrayList<>();
		}
	}
}
