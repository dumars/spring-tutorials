package com.github.dumars.tutorials.web;

import com.github.dumars.tutorials.client.ItemClient;
import com.github.dumars.tutorials.hateoas.ItemResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ItemController {

	@Autowired
	private ItemClient itemClient;

	@GetMapping("/")
	public String index(Model model) {
		ItemResources resources = itemClient.findAll(PageRequest.of(0, 10));
		model.addAttribute("items", resources.getRoot().getItems());
		return "index";
	}

	@GetMapping("/item/{id}")
	public String findItemById(@PathVariable("id") Long id, Model model) {
		model.addAttribute("item", itemClient.findById(id));
		return "detail";
	}
}
