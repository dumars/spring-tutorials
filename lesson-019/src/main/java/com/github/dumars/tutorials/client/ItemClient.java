package com.github.dumars.tutorials.client;

import com.github.dumars.tutorials.domain.Item;
import com.github.dumars.tutorials.hateoas.ItemResources;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "LESSON-018")
public interface ItemClient {

	@RequestMapping(method = RequestMethod.GET, value = "/item")
	ItemResources findAll(Pageable pageable);

	@RequestMapping(method = RequestMethod.GET, value = "/item/search/idLessThan")
	ItemResources findByIdLessThan(@RequestParam("num") Long num);

	@RequestMapping(method = RequestMethod.GET, value = "/item/{id}", consumes = "application/json")
	Item findById(@PathVariable("id") Long id);
}
