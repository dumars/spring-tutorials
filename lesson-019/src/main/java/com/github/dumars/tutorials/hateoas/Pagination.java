package com.github.dumars.tutorials.hateoas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Pagination implements Serializable {

	/**
	 * 目前頁數，即現在是在第幾頁.
	 */
	private int number;

	/**
	 * 分頁大小.
	 */
	private int size;

	/**
	 * 總頁數.
	 */
	private int totalPages;

	/**
	 * 總筆數.
	 */
	private long totalElements;

}