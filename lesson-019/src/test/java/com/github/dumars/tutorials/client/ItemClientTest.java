package com.github.dumars.tutorials.client;

import com.github.dumars.tutorials.domain.Item;
import com.github.dumars.tutorials.hateoas.ItemResources;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ItemClientTest {

	@Autowired
	private ItemClient itemClient;

	@Autowired
	private RestTemplate restTemplate;

	@Test
	public void findAll() {
		Pageable pageable = PageRequest.of(0, 2);
		ItemResources pagination = itemClient.findAll(pageable);
		log.debug("size: {}", pagination.getRoot().getItems().size());

		ItemResources result = restTemplate.getForObject("http://LESSON-018/item?page=0&size=2", ItemResources.class);
		log.debug("pagination: {}", result.getPagination());
	}

	@Test
	public void findById() {
		Item item = itemClient.findById(1L);
		log.debug(ToStringBuilder.reflectionToString(item));

		ResponseEntity<Item> entity = restTemplate.getForEntity("http://LESSON-018/item/1", Item.class);
		log.debug(ToStringBuilder.reflectionToString(entity.getBody()));
	}

	@Test
	public void idLessThan() {
		ItemResources resources = itemClient.findByIdLessThan(3L);
		log.debug(ToStringBuilder.reflectionToString(resources.getRoot()));
	}
}