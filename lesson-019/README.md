# RestTemplate 與 Feign

> 承接 lesson 018 的內容，當有提供服務發現與註冊的功能時，不再是單純的指定固定 IP 或 DNS Name 的方式來連接。

#### Dependencies

```xml
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-commons</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-hateoas</artifactId>
</dependency>
<!-- feign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-okhttp</artifactId>
</dependency>
```

僅列出要說明的部分：

1. spring-data-commons: 這只是因為用到 Pageable 分頁相關的程式所以才加入。

2. spring-boot-starter-hateoas: 請回憶 lesson 012 提到的內容。

3. spring-cloud-starter-openfeign 及 feign-okhttp: rest client 套件。
  
  
## EnableDiscoveryClient

同 lesson 018 一樣要連接 Consul Server，這樣才能取得其他服務的資訊。配置類似 lesson 018，這裡就不再列出。

## RestTemplate

```java
@Bean
@LoadBalanced
RestTemplate restTemplate() {
    return new RestTemplate();
}
```

加上 `@LoadBalanced` 就表示要向 Consul 取得連接對象的資訊，而不是直接使用填寫的 URL/URI 來連接。
除了這部分，其餘用法跟平常使用的 RestTemplate 都一樣。

```java
@Autowired
private RestTemplate restTemplate;
    
restTemplate.getForEntity("http://LESSON-018/item/1", Item.class);
```

`getForEntity`: 表示使用 GET 方法。

`http://LESSON-018/item/1`: `LESSON-018` 這幾個字特別用大寫來表示是為了提醒這個部分會向 Consul 要求
註冊名稱為 lesson-018 的服務提供者資訊，因此大寫或小寫都可以的。

`Item.class`: 由 Spring 封裝成 Item 物件。

實際程式請參考測試程式。

## OpenFeign

OpenFeign 是一個看完文件覺得超棒的東西，真正導入後好想打人的東西。它簡單的設定、
又支援 Ribbon 做到 Load Balance，還支援 Hystrix 可以做統計以及 Hystrix Fallbacks 機制，
真的是太棒了，滿滿的誠意。

但是如果 Server Side 有使用 HATEOAS.....當場跛腳，你沒有辦法偷懶只能遵循 HATEOAS 乖乖實做機制，
因為目前看到的並沒有很好的整合這一部份。

不過如果你是用 MyBatis + RestController 做的服務，那真的非常簡單。

設定：
```yml
feign:
  client:
    config:
      default:
        connect-timeout: 5000
        read-timeout: 5000
        logger-level: full
  httpclient:
    enabled: false
  okhttp:
    enabled: true
  hystrix:
    enabled: true
```

其預設是使用 httpclient 這裡換成 okhttp，並且打開 hystrix 之後有用。另外還有一些其他設定，
有興趣自己去看，它的文件內容算是很少的了。

```java
@Configuration
@ConditionalOnClass(Feign.class)
@AutoConfigureBefore(FeignAutoConfiguration.class)
public class FeignConfig {

    @Bean
    ConnectionPool connectionPool() {
        return new ConnectionPool(50, 5L, TimeUnit.MINUTES);
    }

    @Bean
    OkHttpClient okHttpClient(ConnectionPool connectionPool) {
        return new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .followRedirects(true)
                .readTimeout(3, TimeUnit.MINUTES)
                .retryOnConnectionFailure(false)
                .writeTimeout(3, TimeUnit.MINUTES)
                .connectionPool(connectionPool)
                .build();
    }
}
```

設定 Connection Pool 沒什麼特別的。

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.github.dumars.tutorials.client")
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
```

告訴 Spring 要使用 Feign Client。Hypermedia 的部分其實沒用到，我用偷懶的方法避開實做 HATEOAS。

#### Annotation FeignClient

```java
@FeignClient(value = "LESSON-018")
public interface ItemClient {

	@RequestMapping(method = RequestMethod.GET, value = "/item")
	ItemResources findAll(Pageable pageable);

	@RequestMapping(method = RequestMethod.GET, value = "/item/search/idLessThan")
	ItemResources findByIdLessThan(@RequestParam("num") Long num);

	@RequestMapping(method = RequestMethod.GET, value = "/item/{id}", consumes = "application/json")
	Item findById(@PathVariable("id") Long id);
}
```

`@FeignClient` 原始碼
```java
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FeignClient {
    @AliasFor("name")
    String value() default "";

    /** @deprecated */
    @Deprecated
    String serviceId() default "";

    String contextId() default "";

    @AliasFor("value")
    String name() default "";

    String qualifier() default "";

    String url() default "";

    boolean decode404() default false;

    Class<?>[] configuration() default {};

    Class<?> fallback() default void.class;

    Class<?> fallbackFactory() default void.class;

    String path() default "";

    boolean primary() default true;
}
```

由原始碼可以看到它可以設定的內容很多，因此應該可以滿足絕大部分的系統。

#### 程式

```java
Pageable pageable = PageRequest.of(0, 2);
ItemResources pagination = itemClient.findAll(pageable);
```

真正撰寫程式時真的非常簡單，簡單到不知道要寫什麼了，那就去看看測試程式吧。

#### View

這次加入了 context-path 設定，port 也改成 9000
```yml
server:
  port: 9000
  servlet:
    context-path: /lesson-019
```

因此畫面初始路徑會變成 [http://localhost:9000/](http://localhost:9000/)