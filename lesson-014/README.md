# Quartz 排程快速介紹  

本次介紹 Quartz 的兩種設定：  

1. 單機設定：單機上沒有多台機器同時執行同一個排程的問題，所以設定較少，
只要注意執行時間會不會跨到下次排程啟動的時間就夠了，也可以設定 `@DisallowConcurrentExecution`

2. Cluster 設定：要先建立資料庫 Tables，然後多一些設定。在 `src/documents/tables_mysql.sql`
有 mysql 的資料庫 script，請先執行。  

## Autowired  

雖然說 Spring 有整合 Quartz，但 Quartz 本身並非針對網頁程式來開發的，所以就必須額外做點事才能讓它可以
輕鬆的使用 Spring Bean。  

```java
// AutoWiringSpringBeanJobFactory.java
public class AutoWiringSpringBeanJobFactory extends SpringBeanJobFactory implements ApplicationContextAware {

    private transient AutowireCapableBeanFactory beanFactory;

    public void setApplicationContext(final ApplicationContext context){
        beanFactory = context.getAutowireCapableBeanFactory();
    }

    @Override
    protected Object createJobInstance(final TriggerFiredBundle bundle) throws Exception {
        final Object job = super.createJobInstance(bundle);
        beanFactory.autowireBean(job);
        return job;
    }
}


// QuartzConfig.java
@Bean
AutoWiringSpringBeanJobFactory autoWiringSpringBeanJobFactory() {
	return new AutoWiringSpringBeanJobFactory();
}

@Bean
SchedulerFactoryBean defaultSchedulerFactoryBean(AutoWiringSpringBeanJobFactory autoWiringSpringBeanJobFactory) {
	SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
	scheduler.setJobFactory(autoWiringSpringBeanJobFactory);
	
	...
}
```

## QuartzJobBean  

解決了 autowired 的問題後一切都簡單了，就像寫一般的程式一樣，沒多大差別。

```java
@Component
@DisallowConcurrentExecution
public class PrinterJob extends QuartzJobBean {

	@Value("${spring.quartz.properties.org.quartz.scheduler.instanceName}")
	private String instanceName;

	@Autowired
	private PrinterService printerService;

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		printerService.print(instanceName);
	}
}
```

## 將程式加入排程  

因為比較偷懶，所以建了兩個 private method 來產生 JobDetailFactoryBean 與 CronTriggerFactoryBean，
要注意的只有 `factory.setDurability(true)` 這個設定值，如果是使用 Cluster 的機制，一定要為 true。

```java
@Bean("printerJobDetail")
JobDetailFactoryBean printerJobDetailFactoryBean() {
    return createJobDetail(PrinterJob.class, "PrinterJob");
}

@Bean("printerTrigger")
CronTriggerFactoryBean printerCronTriggerFactoryBean() {
    return createCronTrigger(printerJobDetailFactoryBean(), "PrinterJob", "0/10 * * ? * *");
}

private JobDetailFactoryBean createJobDetail(final Class<? extends Job> clazz, final String name) {
    JobDetailFactoryBean factory = new JobDetailFactoryBean();
    factory.setJobClass(clazz);
    factory.setGroup("default");
    factory.setName(name);
    factory.setDescription(name + "@quartz");
    factory.setDurability(true);
    return factory;
}

private CronTriggerFactoryBean createCronTrigger(final JobDetailFactoryBean jobDetail, final String name, final String cronExpress) {
    CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
    factoryBean.setJobDetail(jobDetail.getObject());
    factoryBean.setStartDelay(1000);
    factoryBean.setGroup("default");
    factoryBean.setName(name);
    factoryBean.setCronExpression(cronExpress);
    return factoryBean;
}
```
  
  
## 建立簡單查詢與控制排程 API

要取得 JobDetail 與 Trigger 最簡單的方法就是直接把 `SchedulerFactoryBean` 拿來，把內容取出來就可以輕鬆獲得所有排程資訊，
詳細內容請看 `QuartzRestController`。

簡易功能說明：  

### 查詢所有排程

```sh
curl http://localhost:8080/schedules
```
```json
[
    {
        "jobName": "PrinterJob",
        "groupName": "default",
        "nextFire": "2019-05-17 20:24:20",
        "description": "PrinterJob@quartz",
        "className": "PrinterJob",
        "startTime": "2019-05-17 20:14:30",
        "state": "NORMAL",
        "previousFire": "2019-05-17 20:24:10"
    }
]
```
  
### 直接啟動排程開始執行

POST `/schedule/{groupName}/{jobName}`
```sh
curl -X POST http://localhost:8080/schedule/default/PrinterJob
```
```text
true
```
  
### 暫停排程

PATCH `/schedule/{groupName}/{jobName}`
```sh
curl -X PATCH http://localhost:8080/schedule/default/PrinterJob
```
```text
true
```
  
這時候查詢排程資料會得到 `"state": "PAUSED"` 的結果
```json
[
    {
        "jobName": "PrinterJob",
        "groupName": "default",
        "nextFire": "2019-05-17 20:31:40",
        "description": "PrinterJob@quartz",
        "className": "PrinterJob",
        "startTime": "2019-05-17 20:28:05",
        "state": "PAUSED",
        "previousFire": "2019-05-17 20:31:30"
    }
]
```

### 回復暫停的排程繼續執行

PUT `/schedule/{groupName}/{jobName}`
```sh
curl -X PUT http://localhost:8080/schedule/default/PrinterJob
```
```text
true
```
  
  
---
  
Quartz 並沒有很複雜的設定與操作，也因此它偏向各自獨立的排程執行方式，
如果是一連串連續的排程建議可以使用 Spring Batch。
