package com.github.dumars.tutorials.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PrinterService {

	public void print(String instanceName) {
		log.info("Execute printer service, quartz instance name: {}", instanceName);
	}
}
