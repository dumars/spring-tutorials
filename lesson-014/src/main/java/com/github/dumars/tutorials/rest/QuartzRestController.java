package com.github.dumars.tutorials.rest;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class QuartzRestController {

	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;

	/**
	 * 查詢所有排程
	 * @return
	 * @throws SchedulerException
	 */
	@GetMapping("/schedules")
	public List<Map<String, String>> getAllJobs() throws SchedulerException {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		List<Map<String, String>> schedules = new ArrayList<>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		for (String groupName : scheduler.getJobGroupNames()) {
			for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
				List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
				Trigger trigger = triggers.get(0);

				JobDetail detail = scheduler.getJobDetail(jobKey);

				Map<String, String> map = new HashMap<>();
				map.put("groupName", groupName);
				map.put("jobName", jobKey.getName());

				if(trigger.getPreviousFireTime() != null)
					map.put("previousFire", format.format(trigger.getPreviousFireTime()));

				map.put("nextFire", format.format(trigger.getNextFireTime()));
				map.put("description", detail.getDescription());
				map.put("className", detail.getJobClass().getSimpleName());
				map.put("state", scheduler.getTriggerState(trigger.getKey()).name());

				if(trigger.getStartTime() != null)
					map.put("startTime", format.format(trigger.getStartTime()));

				if(trigger.getEndTime() != null)
					map.put("endTime", format.format(trigger.getEndTime()));

				schedules.add(map);
			}
		}

		return schedules;
	}

	/**
	 * 直接啟動排程開始執行
	 * @param groupName group name
	 * @param jobName job name
	 * @return Boolean
	 */
	@PostMapping("/schedule/{groupName}/{jobName}")
	public Boolean execute(@PathVariable String groupName, @PathVariable String jobName) {
		log.info("Trigger job: {}/{}", groupName, jobName);
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		try {
			scheduler.triggerJob(JobKey.jobKey(jobName, groupName));
		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	/**
	 * 暫停排程
	 * @param groupName group name
	 * @param jobName job name
	 * @return Boolean
	 */
	@RequestMapping(value = "/schedule/{groupName}/{jobName}", method = RequestMethod.PATCH)
	public Boolean pauseJob(@PathVariable String groupName, @PathVariable String jobName) {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		try {
			scheduler.pauseJob(JobKey.jobKey(jobName, groupName));
		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	/**
	 * 回復暫停的排程繼續執行
	 * @param groupName group name
	 * @param jobName job name
	 */
	@RequestMapping(value = "/schedule/{groupName}/{jobName}", method = RequestMethod.PUT)
	public Boolean resumeJob(@PathVariable String groupName, @PathVariable String jobName) {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		try {
			scheduler.resumeJob(JobKey.jobKey(jobName, groupName));
		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
			return false;
		}
		return true;
	}
}
