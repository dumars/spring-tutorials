package com.github.dumars.tutorials.job;

import com.github.dumars.tutorials.service.PrinterService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@DisallowConcurrentExecution
public class PrinterJob extends QuartzJobBean {

	@Value("${spring.quartz.properties.org.quartz.scheduler.instanceName}")
	private String instanceName;

	@Autowired
	private PrinterService printerService;

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		printerService.print(instanceName);
	}
}
