package com.github.dumars.tutorials.config;

import com.github.dumars.tutorials.job.PrinterJob;
import com.github.dumars.tutorials.spring.AutoWiringSpringBeanJobFactory;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
@AutoConfigureAfter({DataSourceAutoConfiguration.class, MybatisAutoConfiguration.class})
public class QuartzConfig {

	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@Bean("printerJobDetail")
	JobDetailFactoryBean printerJobDetailFactoryBean() {
		return createJobDetail(PrinterJob.class, "PrinterJob");
	}

	@Bean("printerTrigger")
	CronTriggerFactoryBean printerCronTriggerFactoryBean() {
		return createCronTrigger(printerJobDetailFactoryBean(), "PrinterJob", "0/10 * * ? * *");
	}

	@Bean
	AutoWiringSpringBeanJobFactory autoWiringSpringBeanJobFactory() {
		return new AutoWiringSpringBeanJobFactory();
	}

	@Bean
	@Profile("default")
	SchedulerFactoryBean defaultSchedulerFactoryBean(AutoWiringSpringBeanJobFactory autoWiringSpringBeanJobFactory) {
		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
		scheduler.setJobFactory(autoWiringSpringBeanJobFactory);
		scheduler.setAutoStartup(true);
		scheduler.setOverwriteExistingJobs(true);
		scheduler.setWaitForJobsToCompleteOnShutdown(true);
		scheduler.setTriggers(
				printerCronTriggerFactoryBean().getObject()
		);
		return scheduler;
	}

	@Bean
	@Profile("cluster")
	SchedulerFactoryBean clusterSchedulerFactoryBean(AutoWiringSpringBeanJobFactory autoWiringSpringBeanJobFactory) {
		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
		scheduler.setJobFactory(autoWiringSpringBeanJobFactory);
		scheduler.setDataSource(sqlSessionFactory.getConfiguration().getEnvironment().getDataSource());
		scheduler.setAutoStartup(true);
		scheduler.setOverwriteExistingJobs(true);
		scheduler.setWaitForJobsToCompleteOnShutdown(true);
		scheduler.setTriggers(
				printerCronTriggerFactoryBean().getObject()
		);
		return scheduler;
	}

	private JobDetailFactoryBean createJobDetail(final Class<? extends Job> clazz, final String name) {
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(clazz);
		factory.setGroup("default");
		factory.setName(name);
		factory.setDescription(name + "@quartz");
		factory.setDurability(true);
		return factory;
	}

	private CronTriggerFactoryBean createCronTrigger(final JobDetailFactoryBean jobDetail, final String name, final String cronExpress) {
		CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
		factoryBean.setJobDetail(jobDetail.getObject());
		factoryBean.setStartDelay(1000);
		factoryBean.setGroup("default");
		factoryBean.setName(name);
		factoryBean.setCronExpression(cronExpress);
		return factoryBean;
	}
}
