package com.github.dumars.mybatis;

import com.github.dumars.mybatis.callback.MessageCallback;
import lombok.Cleanup;
import lombok.extern.java.Log;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@Log
public class XmlConfigExecutor {

	public static void main(String[] args) throws Exception {
		log.log(Level.INFO, "execute generator....");

		@Cleanup
		InputStream stream = XmlConfigExecutor.class.getResourceAsStream("/generator-config.xml");

		List<String> warnings = new ArrayList<>();
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = cp.parseConfiguration(stream);

		DefaultShellCallback callback = new DefaultShellCallback(true);

		MyBatisGenerator generator = new MyBatisGenerator(config, callback, warnings);
		generator.generate(new MessageCallback());

		if (warnings.size() > 0) {
			log.log(Level.INFO, "---------------- warnings ---------------------");
			for (String warning : warnings) {
				log.log(Level.INFO, warning);
			}
		}
	}
}
