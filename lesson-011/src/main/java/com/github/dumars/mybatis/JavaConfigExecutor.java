package com.github.dumars.mybatis;

import com.github.dumars.mybatis.callback.MessageCallback;
import lombok.extern.java.Log;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.*;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@Log
public class JavaConfigExecutor {

	public static void main(String[] args) throws Exception {
		log.log(Level.INFO, "execute generator....");

		// JavaModelGeneratorConfiguration
		JavaModelGeneratorConfiguration modelConfig = new JavaModelGeneratorConfiguration();
		modelConfig.setTargetPackage("com.github.dumars.db.model");
		modelConfig.setTargetProject("lesson-011/src/main/java");
		modelConfig.addProperty("enableSubPackages", "true");
		modelConfig.addProperty("trimStrings", "true");

		// JavaClientGeneratorConfiguration
		JavaClientGeneratorConfiguration mapperConfig = new JavaClientGeneratorConfiguration();
		mapperConfig.setTargetPackage("com.github.dumars.db.mapper");
		mapperConfig.setTargetProject("lesson-011/src/main/java");
		mapperConfig.setConfigurationType("ANNOTATEDMAPPER");
		mapperConfig.addProperty("enableSubPackages", "true");

		// CommentGeneratorConfiguration
		CommentGeneratorConfiguration commentConfig = new CommentGeneratorConfiguration();
		commentConfig.addProperty("addRemarkComments", "true");

		// JdbcConnectionConfiguration
		JDBCConnectionConfiguration jdbcConfig = new JDBCConnectionConfiguration();
		jdbcConfig.setDriverClass("com.mysql.jdbc.Driver");
		jdbcConfig.setConnectionURL("jdbc:mysql://localhost:3306/oauth?useSSL=false&amp;serverTimezone=UTC&amp;zeroDateTimeBehavior=round&amp;nullCatalogMeansCurrent=true");
		jdbcConfig.setUserId("root");
		jdbcConfig.setPassword("p@ssw0rd");

		// JavaTypeResolverConfiguration
		JavaTypeResolverConfiguration typeConfig = new JavaTypeResolverConfiguration();
		typeConfig.addProperty("forceBigDecimals", "true");

		Context context = new Context(ModelType.FLAT);
		context.setId("lesson-011");
		context.setTargetRuntime("MyBatis3DynamicSql");
		context.setJavaModelGeneratorConfiguration(modelConfig);
		context.setJavaClientGeneratorConfiguration(mapperConfig);
		context.setCommentGeneratorConfiguration(commentConfig);
		context.setJdbcConnectionConfiguration(jdbcConfig);
		context.setJavaTypeResolverConfiguration(typeConfig);
		context.addProperty("javaFileEncoding", "UTF-8");

		// PluginConfiguration
		PluginConfiguration serializablePlugin = new PluginConfiguration();
		serializablePlugin.setConfigurationType("org.mybatis.generator.plugins.SerializablePlugin");
		context.addPluginConfiguration(serializablePlugin);

		// TableConfiguration
		TableConfiguration item = new TableConfiguration(context);
		GeneratedKey itemKey = new GeneratedKey("id", "MySql", true, "post");
		item.setGeneratedKey(itemKey);
		item.setTableName("item");
		item.setDomainObjectName("Item");
		context.addTableConfiguration(item);

		Configuration config = new Configuration();
		config.addContext(context);

		DefaultShellCallback callback = new DefaultShellCallback(true);
		List<String> warnings = new ArrayList<>();

		MyBatisGenerator generator = new MyBatisGenerator(config, callback, warnings);
		generator.generate(new MessageCallback());

		if (warnings.size() > 0) {
			log.log(Level.INFO, "---------------- warnings ---------------------");
			for (String warning : warnings) {
				log.log(Level.INFO, warning);
			}
		}
	}
}
