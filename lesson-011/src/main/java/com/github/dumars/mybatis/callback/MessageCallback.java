package com.github.dumars.mybatis.callback;

import lombok.extern.java.Log;
import org.mybatis.generator.api.ProgressCallback;

@Log
public class MessageCallback implements ProgressCallback {

	@Override
	public void introspectionStarted(int totalTasks) {
		log.info("Introspection Started: " + totalTasks);
	}

	@Override
	public void generationStarted(int totalTasks) {
		log.info("Generation Started: " + totalTasks);
	}

	@Override
	public void saveStarted(int totalTasks) {
		log.info("Save Started: " + totalTasks);
	}

	@Override
	public void startTask(String taskName) {
		log.info("Task Name: " + taskName);
	}

	@Override
	public void done() {
		log.info("finished.");
	}

	@Override
	public void checkCancel() throws InterruptedException {
		log.warning("fail or cancel.");
	}

}
