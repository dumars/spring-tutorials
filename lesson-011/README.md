# MyBatis Generator 簡單介紹

在程式中設定了兩個執行程式說明如下：

1. XmlConfigExecutor: 使用傳統的 XML 檔案設定方式，設定檔案為 `resources/generator-config.xml`，
產生內容如同以前的 Model、Interface Mapper 以及 XML 檔案。

2. JavaConfigExecutor: 新的設定方法，純 Java 語法，但是完全對應 XML 的模式，
產生內容使用新的 MyBatis Dynamic SQL 語法，詳細內容請看 [MyBatis Dynamic SQL](https://github.com/mybatis/mybatis-dynamic-sql)。

---

設定內容請參考官網說明 [MyBatis Generator](http://www.mybatis.org/generator/index.html)

這內容沒什麼可以說的，就是看說明一個蘿蔔一個坑，對照著設定而已。
