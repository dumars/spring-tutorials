package com.github.dumars.tutorials.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dumars.tutorials.interceptor.entity.OperationRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OperationService {

	@Autowired
	private ObjectMapper objectMapper;

	/**
	 * 對結果進行你想要的操作，存到資料庫、寫到 log file、傳送到其他儲存工具(如：elastic)...等等.
	 * <p>
	 * 以下是單純的寫到 log, 可以針對此 package 設定新的 logback 設定，寫入另一個名稱的 log file，
	 * 這樣就可以跟原本的 log file 分開紀錄。
	 * </p>
	 * <p>
	 * 至於紀錄的格式，範例是單純的用 ObjectMapper 轉成 JSON 格式來儲存，
	 * 可針對自己需求格式化成自己要的。
	 * </p>
	 *
	 * @param record
	 */
	@Async
	public void save(final OperationRecord record) {
		try {
			log.info(objectMapper.writeValueAsString(record));
		} catch (JsonProcessingException e) {
			log.error(e.getMessage(), e);
		}
	}
}
