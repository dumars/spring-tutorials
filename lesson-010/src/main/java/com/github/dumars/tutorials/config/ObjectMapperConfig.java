package com.github.dumars.tutorials.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.TimeZone;

@Configuration
public class ObjectMapperConfig {

	/**
	 * 加入序列化設定，避免直接被轉成數字表達方式，以及加入 TimeZone 在日期之後。
	 * @return ObjectMapper
	 */
	@Primary
	@Bean
	ObjectMapper objectMapper() {
		// @formatter:off
		return new ObjectMapper()
				.registerModule(new ParameterNamesModule())
				.registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule())
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
				.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)
				.setTimeZone(TimeZone.getDefault());
		// @formatter:on
	}

	/**
	 * 加入序列化設定，避免直接被轉成數字表達方式，以及加入 TimeZone 在日期之後， 並強制取消 JSON 欄位名稱的雙引號。
	 * @return ObjectMapper
	 */
	@Bean(name = "noQuoteFieldNameObjectMapper")
	ObjectMapper noQuoteFieldNameObjectMapper() {
		// @formatter:off
		return new ObjectMapper()
				.registerModule(new ParameterNamesModule())
				.registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule())
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
				.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)
				.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false)
				.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
				.setTimeZone(TimeZone.getDefault());
		// @formatter:on
	}
}
