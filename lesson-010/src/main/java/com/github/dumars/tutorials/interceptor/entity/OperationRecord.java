package com.github.dumars.tutorials.interceptor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 資料庫操作紀錄
 */
@Data
@Builder
public class OperationRecord implements Serializable {

	private String sql;

	private String method;

	private String parameters;

	private Long spendTimeMillis;

	private Long userId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime executionTime;

}
