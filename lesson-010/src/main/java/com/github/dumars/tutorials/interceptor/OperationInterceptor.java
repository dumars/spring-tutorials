package com.github.dumars.tutorials.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dumars.tutorials.interceptor.entity.OperationRecord;
import com.github.dumars.tutorials.service.OperationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * MyBatis 操作紀錄攔截器.
 * <p>
 * 攔截 Executor 這個工作階段的資料，從其中取出 SQL 語法、參數名稱與其值，
 * 並計算執行所花時間等等。
 * </p>
 *
 * @author dumars
 */
@Slf4j
@Component
@Intercepts({
		@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class }),
		@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class }),
		@Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }) })
public class OperationInterceptor implements Interceptor {

	/**
	 * 此類型的 ObjectMapper 會去掉 json 欄位名稱的雙引號
	 */
	@Autowired
	@Qualifier("noQuoteFieldNameObjectMapper")
	private ObjectMapper objectMapper;

	@Autowired
	private OperationService operationService;

	/**
	 * 攔截器主程式
	 *
	 * @param invocation
	 * @return
	 * @throws Throwable
	 */
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		log.info("into mybatis executor interceptor.");
		long start = System.currentTimeMillis();

		try {
			return invocation.proceed();
		}
		finally {
			long finish = System.currentTimeMillis();
			log.debug("executing sql spend time: {}ms", (finish - start));

			MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
			SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();

			Object parameters = invocation.getArgs()[1];
			String sqlCommand = getSqlCommand(mappedStatement, parameters);

			OperationRecord record = OperationRecord.builder()
					.sql(sqlCommand).method(sqlCommandType.name())
					.spendTimeMillis(finish - start)
					.executionTime(LocalDateTime.now())
					.parameters(objectMapper.writeValueAsString(parameters))
					.userId(1L).build();
			log.debug("mybatis execution record is {}", record);

			operationService.save(record);
		}
	}

	private String getSqlCommand(final MappedStatement statement, final Object object) {
		if (statement != null && statement.getBoundSql(object) != null) {
			String sql = statement.getBoundSql(object).getSql();
			sql = replaceEach(sql, new String[]{"\n", "\r\n"}, new String[]{" ", " "});
			final String doubleSpace = "  ";
			while (contains(sql, doubleSpace)) {
				sql = replace(sql, doubleSpace, " ");
			}
			return sql;
		}
		return null;
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}
}
