package com.github.dumars.tutorials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 執行 spring-boot:run 的程式啟動點.
 * <p>
 * <code>@SpringBootApplication</code> 會觸發 spring 的 AutoConfiguration 設定
 * </p>
 *
 * @author dumars
 */
@SpringBootApplication
@EnableAsync
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
