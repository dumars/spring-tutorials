
# MyBatis Interceptor - 資料庫操作紀錄  
  
MyBatis 攔截器最常看到的應用是在分頁套件上，網路上可以找到非常多的教學，  
要實現分頁要做的事情比較多也比較複雜，但是如果要記錄資料庫的操作，那就簡單很多。  
  
## Interceptors  
  
MyBatis 的攔截器類型有四種，分別對應不同執行階段與工作內容

1. Executor (update, query, flushStatements, commit, rollback, getTransaction, close, isClosed)  
2. ParameterHandler (getParameterObject, setParameters)  
3. StatementHandler (prepare, parameterize, batch, update, query)  
4. ResultSetHandler (handleResultSets, handleOutputParameters)  

其發生的順序也是如上的順序，這次將以 Executor 為例簡單的紀錄資料庫操作。

### 攔截器宣告

```java
@Intercepts({  
      @Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class }),  
      @Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class }),  
      @Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }) })
```

上面的設定做了什麼呢？

- `@Interceptors` ：使用 MyBatis Interceptors，然後要告訴 MyBatis 要用那個攔截器，攔截那些方法。
- `@Signature`：
    - type：指定攔截器類型，最上面寫的那四種類型的其中一種
    - method：Executor 裡面有很多 Methods，你要攔截的是哪一個 Method
    - args：因為有 Overload Method，所以要告訴 MyBatis 這個 Method 裡面有那些參數類型，它才可以找到正確的 Method

### 實做 Interceptor interface

```java
public interface Interceptor {  
  
  Object intercept(Invocation invocation) throws Throwable;  
  
  Object plugin(Object target);  
  
  void setProperties(Properties properties);  
  
}
```

```java
public class OperationInterceptor implements Interceptor {
    ....略
}
```

這裡比較麻煩的就是搞清楚 Invocation 是什麼，有什麼內容，剩下的就沒什麼困難的地方了。

詳細說明請看程式碼。  

### Annotation EnableAsync and Async

為了避免影響原始流程的流暢度，所以在 OperationService.save(...) 上加入了 `@Async` ，這樣此方法在執行時就會以非同步(異步)的方式執行，而為了讓非同步生效就需要告訴 Spring 要開啟這個功能，因此在 Application 中加入 `@EnableAsync`

```java
// OperationService.java
@Async  
public void save(final OperationRecord record) {  
   try {  
      log.info(objectMapper.writeValueAsString(record));  
   } catch (JsonProcessingException e) {  
      log.error(e.getMessage(), e);  
   }  
}

// Application.java
@SpringBootApplication  
@EnableAsync  
public class Application {  
  
   public static void main(String[] args) {  
      SpringApplication.run(Application.class, args);  
   }  
  
}
```

### 補充說明

如果專案中有使用 MyBatis 第三方分頁套件或是有用到 Executor Interceptor 方法來改變 SQL 條件的套件，
請改用 StatementHandler 的方式來實做，否則這些額外的變化無法被正確解析，切記。