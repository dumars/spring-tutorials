# Spring Bean 簡單介紹
  
>  Spring Bean 是一個被實例化、組裝，並通過 Spring IoC 容器所管理的對象。  
  
Spring Bean 在使用 XML 設定方式與 Java 方式有些差異，因為現在都已經進入~~無紙化時代~~ 使用 Java 設定方式為主流的時代，所以僅介紹這種方式。

舉例來說：以常見的 `ObjectMapper` 來說，假設我要寫一個套件最後打包成 jar 供不同系統使用，因為有新舊系統所以要能符合新系統(Java8+、又有使用 LocalDateTime)，舊系統可能只有 Java5，所以在不同系統上的 ObjectMapper Bean 要有所不同，那我可能就會用以下的方式來初始化：
```java
@Configuration  
@ConditionalOnClass(ObjectMapper.class)  
public class ObjectMapperConfig {  
  
   @Bean  
   @ConditionalOnClass({JavaTimeModule.class})  
   ObjectMapper javaTimeObjectMapper() {  
      return new ObjectMapper()  
            .registerModule(new ParameterNamesModule())  
            .registerModule(new Jdk8Module())  
            .registerModule(new JavaTimeModule())  
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)  
            .configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)  
            .setTimeZone(TimeZone.getDefault());  
   }  
  
   @Bean  
   @ConditionalOnMissingClass({"com.fasterxml.jackson.datatype.jsr310.JavaTimeModule"})  
   ObjectMapper simpleObjectMapper() {  
      return new ObjectMapper();  
   }  
}
```

1. Class Level 的 `@ConditionalOnClass`：先判斷是否有 ObjectMapper 這個 Class，如果沒有下面都不需要了。
2. Method Level 的 `@ConditionalOnClass`：再判斷是否有 JavaTimeModule 這個 Class，這是針對 Java8 的套件，如果有這個 Class，那就會建立 Spring Bean。
3. `@ConditionalOnMissingClass`：這是針對上面的補遺，當沒有 JavaTimeModule 前一個 Spring Bean 就不會存在，因此就會執行這段，建立最基本的 ObjectMapper 提供給舊系統用。

當然還有其他的判斷方式可以用，這只是其中兩種。


## Bean Scope
- singleton
- prototype
- request
- session
- application
- websocket

`BeanScopeConfig` 中建立了 singleton 及 prototype 兩種類型的 Bean，你可以在不同頁面中切換("http://localhost:8080/", "http://localhost:8080/item")，然後再去看 log，你會發現 prototype 類型的 Bean 所印出的時間會有不同結果。


## lazy-initialization
`@Lazy(true)` or `@Lazy(false)` 預設是 true，也就是說 web 啟動時不會馬上建立此物件，真正用到時才會執行初始化。

---

還有一些比較少用到的，自己有興趣可以去看看，至少得知道還有什麼。