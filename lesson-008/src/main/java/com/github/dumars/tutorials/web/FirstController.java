package com.github.dumars.tutorials.web;

import com.github.dumars.tutorials.config.BeanScopeConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Spring MVC.
 *
 * @author dumars
 */
@Slf4j
@Controller
public class FirstController {

	@Autowired
	@Qualifier("singletonScopeBean")
	private BeanScopeConfig.ScopeBean singletonScopeBean;

	@Autowired
	@Qualifier("prototypeScopeBean")
	private BeanScopeConfig.ScopeBean prototypeScopeBean;

	/**
	 * <code>@GetMapping</code> 亦同 <code>@RequestMapping(method = RequestMethod.GET)</code>
	 * <code>@GetMapping</code> 也就是只接受 GET 的方式來操作此方法
	 *
	 * @return 讀取 /resources/templates/index.html
	 */
	@GetMapping("/")
	public String index() {
		log.debug("singleton: {}", singletonScopeBean.getTime());
		log.debug("prototype: {}", prototypeScopeBean.getTime());
		return "index";
	}
}
