package com.github.dumars.tutorials.config;

import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import java.time.LocalDateTime;

@Configuration
public class BeanScopeConfig {

	@Primary
	@Bean("singletonScopeBean")
	@Scope("singleton")
	ScopeBean singletonScopeBean() {
		return new ScopeBean();
	}

	@Bean("prototypeScopeBean")
	@Scope("prototype")
	ScopeBean prototypeScopeBean() {
		return new ScopeBean();
	}

	@Data
	public static class ScopeBean {

		private LocalDateTime time;

		public ScopeBean() {
			this.time = LocalDateTime.now();
		}
	}
}
