package com.github.dumars.tutorials.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dumars.tutorials.config.BeanScopeConfig;
import com.github.dumars.tutorials.domain.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Spring MVC and first form.
 *
 * @author dumars
 */
@Slf4j
@Controller
public class FirstFormController {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Qualifier("singletonScopeBean")
	private BeanScopeConfig.ScopeBean singletonScopeBean;

	@Autowired
	@Qualifier("prototypeScopeBean")
	private BeanScopeConfig.ScopeBean prototypeScopeBean;

	/**
	 * 開啟新增商品的輸入表單
	 * @return
	 */
	@GetMapping("/item")
	public String index() {
		log.debug("singleton: {}", singletonScopeBean.getTime());
		log.debug("prototype: {}", prototypeScopeBean.getTime());
		return "/item/creation";
	}

	/**
	 * 儲存商品資料，因為使用了 redirect 機制，所以須使用 RedirectAttributes 來將資料帶到下一個環節
	 * @param item 商品內容
	 * @param attributes {@Link RedirectAttributes}
	 * @return
	 */
	@PostMapping("/item")
	public String save(Item item, RedirectAttributes attributes) {
		item.setId(1L);
		log.debug("Saving item: {}", item);
		attributes.addFlashAttribute("item", item);
		return "redirect:/item/1";
	}

	/**
	 * 顯示商品明細
	 * @param id
	 * @return
	 */
	@GetMapping("/item/{id}")
	public String findItemById(@PathVariable("id") Long id) {
		log.debug("find item by id: {}", id);
		return "/item/detail";
	}
}
