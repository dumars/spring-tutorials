package com.github.dumars.tutorials.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.TimeZone;

@Configuration
@ConditionalOnClass(ObjectMapper.class)
public class ObjectMapperConfig {

	@Bean
	@ConditionalOnClass({JavaTimeModule.class})
	ObjectMapper javaTimeObjectMapper() {
		return new ObjectMapper()
				.registerModule(new ParameterNamesModule())
				.registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule())
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
				.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)
				.setTimeZone(TimeZone.getDefault());
	}

	@Bean
	@ConditionalOnMissingClass({"com.fasterxml.jackson.datatype.jsr310.JavaTimeModule"})
	ObjectMapper simpleObjectMapper() {
		return new ObjectMapper();
	}
}
