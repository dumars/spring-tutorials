package com.github.dumars.tutorials.config;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.resource.ClientResources;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;

import java.util.HashSet;
import java.util.Set;

@Profile("sentinel")
@Configuration
public class SentinelRedisConfig {

	@Autowired
	private RedisProperties properties;

	@Bean
	ClientOptions clientOptions() {
		return ClientOptions.builder()
				.disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS)
				.autoReconnect(true).build();
	}

	@Bean
	LettucePoolingClientConfiguration lettucePoolConfig(ClientResources clientResources) {
		GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
		poolConfig.setMaxIdle(properties.getLettuce().getPool().getMaxIdle());
		poolConfig.setMinIdle(properties.getLettuce().getPool().getMinIdle());
		poolConfig.setMaxTotal(properties.getLettuce().getPool().getMaxActive());
		poolConfig.setMaxWaitMillis(properties.getLettuce().getPool().getMaxWait().toMillis());
		// @formatter:off
		return LettucePoolingClientConfiguration.builder()
				.poolConfig(poolConfig)
				.clientOptions(clientOptions())
				.clientResources(clientResources)
				.build();
		// @formatter:on
	}

	@Bean
	RedisConnectionFactory redisConnectionFactory(
			LettucePoolingClientConfiguration lettucePoolConfig) {
		Set<RedisNode> redisNodes = new HashSet<>();
		properties.getSentinel().getNodes().forEach(setting -> {
			redisNodes.add(new RedisNode(setting.split(":")[0],
					Integer.parseInt(setting.split(":")[1])));
		});

		RedisSentinelConfiguration config = new RedisSentinelConfiguration();
		config.setDatabase(properties.getDatabase());
		config.setPassword(properties.getPassword());
		config.setSentinels(redisNodes);
		config.master(properties.getSentinel().getMaster());
		return new LettuceConnectionFactory(config, lettucePoolConfig);
	}
}
