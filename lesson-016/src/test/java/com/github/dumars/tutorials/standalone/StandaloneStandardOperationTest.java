package com.github.dumars.tutorials.standalone;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class StandaloneStandardOperationTest {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Resource(name = "stringRedisTemplate")
	private ValueOperations<String, String> valueOps;

	@Resource(name = "stringRedisTemplate")
	private SetOperations<String, String> setOps;

	/**
	 * String value 操作測試.
	 */
	@Test
	public void testStringValueOperation() {
		assertNotNull(stringRedisTemplate);
		assertNotNull(valueOps);

		valueOps.set("foo", "bar");
		assertTrue(stringRedisTemplate.hasKey("foo"));
		assertEquals(valueOps.get("foo"), "bar");

		valueOps.append("foo", "bar");
		assertEquals(valueOps.get("foo"), "barbar");

		assertTrue(stringRedisTemplate.delete("foo"));
		assertFalse(stringRedisTemplate.hasKey("foo"));
	}

	/**
	 * String set 操作測試.
	 * 請記得 Set 是無序的.
	 */
	@Test
	public void testStringSetOperation() {
		assertNotNull(stringRedisTemplate);
		assertNotNull(setOps);

		setOps.add("foo-set", "bar", "bar2", "bar3");
		assertTrue(setOps.isMember("foo-set", "bar2"));
		assertFalse(setOps.isMember("foo-set", "ooxx"));

		assertTrue(setOps.move("foo-set", "bar2", "foo-set2"));
		assertFalse(setOps.isMember("foo-set", "bar2"));
		assertTrue(setOps.isMember("foo-set2", "bar2"));

		setOps.remove("foo-set2", "bar2");
		assertTrue(setOps.size("foo-set2") == 0);

		setOps.remove("foo-set", "bar");
		assertTrue(setOps.size("foo-set") == 1);
		assertTrue(setOps.isMember("foo-set", "bar3"));

		setOps.add("foo-set2", "bar4", "bar5");
		setOps.unionAndStore("foo-set", "foo-set2", "foo-set3");
		assertTrue(setOps.isMember("foo-set3", "bar3"));
		assertTrue(setOps.isMember("foo-set3", "bar4"));
		assertTrue(setOps.isMember("foo-set3", "bar5"));

		stringRedisTemplate.delete("foo-set");
		stringRedisTemplate.delete("foo-set2");
		stringRedisTemplate.delete("foo-set3");
	}
}
