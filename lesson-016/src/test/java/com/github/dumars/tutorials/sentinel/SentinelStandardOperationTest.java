package com.github.dumars.tutorials.sentinel;

import com.github.dumars.tutorials.standalone.StandaloneStandardOperationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("sentinel")
public class SentinelStandardOperationTest extends StandaloneStandardOperationTest {
}
