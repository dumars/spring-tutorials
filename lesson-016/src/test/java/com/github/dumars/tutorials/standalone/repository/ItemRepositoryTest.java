package com.github.dumars.tutorials.standalone.repository;

import com.github.dumars.tutorials.domain.Item;
import com.github.dumars.tutorials.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ItemRepositoryTest {

	@Autowired
	private ItemRepository itemRepository;

	@Test
	public void testItemOperation() {
		Item item = new Item();
		item.setId(1L);
		item.setName("item 001");
		item.setPrice(300F);

		itemRepository.save(item);

		Optional<Item> optional = itemRepository.findById("1");
		log.debug(optional.toString());
		assertTrue(optional.isPresent());

		item.setName("item new name");
		item = itemRepository.save(item);
		log.debug(item.toString());

		optional = itemRepository.findById("1");
		optional.ifPresent(value -> assertEquals("item new name", value.getName()));

		itemRepository.delete(item);
	}
}
