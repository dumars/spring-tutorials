# Spring Data Redis 介紹
  
這個章節要用到 Redis，所以請先設定好環境，在 `src/documents` 之下有設定檔以及 docker-compose 啟動檔案，接下來會依序介紹。  
  
## Redis Standalone
  
Standalone 顧名思義就是指啟動一個 Redis Server，對應的檔案為 `compose-redis-standalone.yml`，
如果只是單純的要啟動一個 Redis，而資料都不要保存的話，可以用以下內容  
  
```yml
version: "3.3"

services:
  redis:
    image: "redis:5.0.3"
    container_name: cloud-redis
    command: redis-server
    ports:
      - "6379:6379"
```
  
用預設的設定就很足夠，也不用設定 volumes，簡單又快速，如果是想保留資料，那就用原本的設定，
然後執行 `docker-compose -f compose-redis-standalone.yml up -d`  
  
## Redis Sentinel
  
Sentinel 機制有很多可以說，但是這裡只簡單的介紹
  
1. master-slave 機制：建立一個 master 以及多個 slave，在 `compose-redis-standalone.yml` 我們設定了兩個 slave，
請修改設定檔中的資料夾路徑，並將 conf 設定檔放到正確的路徑上。
  
2. sentinel 機制：這個一樣要起動多個實體，用的是 redis-sentinel 而不再是 redis-server，sentinel 的作用是要監控
上面的 master-slave 群組，而且可以監控不同的、多個 master-slave 群組，因此在 conf 設定中要設定每個群組的名稱，
這樣 client 在連線時 sentinel 才會知道要給你哪一組的網路資料。

在 Windows 及 Mac OS 請不用執行測試程式來跑這個環境，因為 Docker 網路設定的關係， sentinel 會返回一個
Docker 建立的內部網路 IP，除非你有固定 IP 或者是用 network mode = Host 的方式，否則會連不到。

你可以使用 client 工具來試試看，操作 master 然後看看 slave 是否有做相同的操作。
  
## Jedis or Lettuce
  
Spring Data Redis 預設支援兩種 client 套件，就是 Jedis 及 Lettuce，最早是只有 Jedis 後來為了可以
使用 Reactive 這種非阻塞式 IO 的執行方式所以加入了 Lettuce，為了以後可能會用到 Reactive Redis 
所以我們選擇 Lettuce。
  
## Dependencies

```xml
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-redis</artifactId>
</dependency>
<dependency>
    <groupId>io.lettuce</groupId>
    <artifactId>lettuce-core</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-pool2</artifactId>
</dependency>
```
  
因為有使用 Connection Pool 機制，所以加入 `commons-pool2`
  
## 設定
  
```yml
spring:
  profiles: default
  redis:
    database: 0
    host: localhost
    port: 6379
    password:
    lettuce:
      pool:
        max-idle: 20
        min-idle: 10
        max-active: 20
        max-wait: 10s
```
  
為了方便測試，所以有 default 及 sentinel 兩種 profiles，在執行測試程式時可以方便載入不同設定，
這裡唯一要提的就是 `database` 這個設定，Redis Server 預設會啟動16個 database，數字從 0 ~ 15，
這樣不同系統可以選擇不同 database 從而避免資料混亂夾雜在一起的問題。

## Spring Configuration

```java
@Profile("default")
@Configuration
public class StandaloneRedisConfig {
    ...
}
``` 
   
```java
@Profile("sentinel")
@Configuration
public class SentinelRedisConfig {
    ...
}
```
  
前面有提到 Profiles 的設定，所以這裡也定義了 `@Profile`，兩個設定內容只有些微的差異。  
  
```java
@SpringBootApplication
@EnableRedisRepositories(basePackages = {"com.github.dumars.tutorials.repository"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
```
  
`@EnableRedisRepositories` 是設定使用 spring redis repository 的機制，其內容非常類似之前我們使用
JPA 時也有用到的 repository，這裡有一點要特別注意的部分：
  
> 當專案裡有使用其他不同類型的連線所建立的 `@Repository` 時，例如資料庫、Elasticsearch...等等，
必須要各自設定各自的 basePackages 否則會造成錯誤，雖然連線都叫 Connection，但是資料庫的連線與
 Redis 的連線那是完全不同的，其 annotation 名稱格式大概會是 EnableXxxxxRepository。
  
```java
@Data
@RedisHash("item")
public class Item {

	@Id
	private Long id;
	private String name;
	private Float price;

}
```

`@RedisHash("item")` 是為了定義 Redis Key 的名稱，`@Id` 是定義這個資料的 Key 值欄位。

## 測試

所有的 Redis 操作都寫在測試程式中，你可以試著使用不同類型的 Operation 來操作並用 client 工具來檢視結果。
  
Spring 所提供的 RedisTemplate API 並沒有覆蓋 Redis 所有的功能，你可以在 [官方文件](https://docs.spring.io/spring-data/redis/docs/2.1.8.RELEASE/reference/html/#_supported_commands)
中看到它所能做的操作。

## 後記

Redis 所提供的功能還有很多，細節設定也是，因此這部分要靠你自己去閱讀跟學習，這裡只是個簡單介紹。