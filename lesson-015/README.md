# 來發個 E-mail 吧

1. VerySimpleMailService: 最陽春的電子郵件發送，雖然陽春但是單筆簡易類型的郵件類型就很足夠了。

2. SimpleMailService: 一樣很簡單的電子郵件發送，多了附件以及在郵件內容顯示的功能。

3. TemplateMailService: 剩下的所有程式都跟這個有關，這部分的內容幾乎完全是從 Thymeleaf 官方範例複製過來的，
位置在 [Thymeleaf 3 examples: Spring Mail](https://github.com/thymeleaf/thymeleafexamples-springmail)

## Spring Java Mail 設定

```yml
spring:
  mail:
    host: smtp.gmail.com
    port: 587
    username: ${mail.account}
    password: ${mail.password}
    protocol: smtp
    defaultEncoding: UTF-8
    properties:
      mail:
        smtp:
          auth: true
          connectiontimeout: 5000
          timeout: 3000
          writetimeout: 5000
          starttls:
            enable: true
            required: true
```

以上設定是 Gmail 的設定數值  

username 及 password 請更換為自己可以測試的帳號(username 要輸入完整的 gmail 帳號)，
或是在啟動時用參數名稱代入 `mvn spring-boot:run -Dspring-boot.run.arguments=--mail.account=oooo,--mail.password=xxxxx`
  
## Thymeleaf Template

操作畫面：`http://localhost:8080`  

進入畫面操作一遍大概就了解其內容了。
