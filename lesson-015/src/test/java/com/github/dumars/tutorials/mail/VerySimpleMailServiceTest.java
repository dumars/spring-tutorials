package com.github.dumars.tutorials.mail;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.AddressException;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class VerySimpleMailServiceTest {

	@Autowired
	private VerySimpleMailService verySimpleMailService;

	@Test
	public void send() throws AddressException {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("your-account@gmail.com");
		message.setTo("who@gmail.com");
		message.setSubject("Java Mail");
		message.setText("Dear John Doe, 工時記得要打！！");

		verySimpleMailService.send(message);
	}
}