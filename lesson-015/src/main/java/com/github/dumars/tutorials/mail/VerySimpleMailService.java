package com.github.dumars.tutorials.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * 最簡單的郵件發送.
 */
@Slf4j
@Service
public class VerySimpleMailService {

	@Autowired
	private JavaMailSender mailSender;

	/**
	 * 發送郵件，僅適合單一郵件發送
	 * @param message {@Link SimpleMailMessage}
	 */
	public void send(SimpleMailMessage message) {
		try{
			this.mailSender.send(message);
		} catch (MailException ex) {
			log.error(ex.getMessage(), ex);
		}
	}
}
