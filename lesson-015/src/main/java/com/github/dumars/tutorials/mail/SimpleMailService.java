package com.github.dumars.tutorials.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.lang.NonNull;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.UUID;

/**
 * 簡單的郵件發送並附加檔案.
 */
@Slf4j
@Service
public class SimpleMailService {

	private static final String IMAGE_PATH = "/static/images/001.jpg";

	@Autowired
	private JavaMailSender mailSender;

	public void send(@NonNull final SimpleMailMessage message) throws MessagingException {
		assert message.getFrom() != null;
		assert message.getTo() != null;
		assert message.getSubject() != null;

		MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		helper.setFrom(message.getFrom());
		helper.setTo(message.getTo());
		helper.setSubject(message.getSubject());
		helper.setText(message.getText());

		InputStreamResource resource = new InputStreamResource(
				SimpleMailService.class.getResourceAsStream(IMAGE_PATH));
		// attachment
		helper.addAttachment("CoolImage.jpg", resource);
		// inline
		helper.addInline(UUID.randomUUID().toString(), resource);

		this.mailSender.send(message);
	}
}
