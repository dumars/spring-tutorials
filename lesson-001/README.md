# Lesson 1: spring-boot-start-web

spring-boot-start-web 其中已經整合了最基本的網頁相關套件，因此只需要此套件就可以開始寫最簡單的網頁應用程式。

maven 設定範例
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>

<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

## Dependencies
1. spring-boot-starter-web: 最基本的網頁應用程式套件
2. spring-boot-starter-thymeleaf: thymeleaf 網頁模板
3. spring-boot-starter-test: spring 測試用套件

## Plugins
1. spring-boot-maven-plugin: spring 在 maven 環境的執行套件

## 啟動 Web Application
```sh
mvn spring-boot:run
```
[打開第一個畫面](http://localhost:8080/)