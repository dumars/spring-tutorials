# Tutorials

#### 建議環境：  
- jdk: 8+  
- maven: 3.5+  
- docker: 18+  
- git: 2+  
- IDE 強烈推薦使用 Intellij  

#### 套件版本：  
- spring-boot: 2.1.4.RELEASE  
- spring-cloud: Greenwich.SR1  
  
  
開始之前記得先檢查環境與設定，包含檢查一遍各個套件的版本，避免遇到各種不明原因白白浪費大把時間。

---
  
## 快速導覽

1. lesson 001: 建立第一個 spring web application  
2. lesson 002: 建立第一個表單及其操作，介紹 lombok、spring logging 以及 devtools  
3. lesson 003: Docker 介紹與簡單操作  
4. lesson 004: spring + mybatis 整合  
5. lesson 005: Spring properties 的運用  
6. lesson 006: Spring Security 的簡單使用  
7. lesson 007: 讓畫面好看一點吧！Thymeleaf Layout 及 Webjars 介紹  
8. lesson 008: Spring Bean 簡單介紹  
9. lesson 009: Spring Handler Interceptor  
10. lesson 010: MyBatis Interceptor - 資料庫操作紀錄  
11. lesson 011: MyBatis Generator 簡單介紹  
12. lesson 012: 來寫個 REST API 吧(JPA 版本)  
13. lesson 013: 來寫個 REST API 吧(MyBatis 版本)  
14. lesson 014: Quartz 排程快速介紹  
15. lesson 015: 來發個 E-mail 吧  
16. lesson 016: Spring Data Redis 介紹  
17. lesson 017: Spring Session Data Redis  
18. lesson 018: 服務註冊與發現  
19. lesson 019: RestTemplate 與 Feign
